﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConexGameLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Globalization;
using SharpDX.DirectInput;
using Microsoft.Xna.Framework.Input;
using System.Media;

namespace Conex_Game_Test01
{
    public partial class MainGame : Microsoft.Xna.Framework.Game
    {
        #region GAME SEGMENT FUNCTIONS

        public void Create_PlayerLoadInfo(GameSegment seg)
        {
            seg.playerLoadInfos.Add(new PlayerLoadInfo(
                   "Test Player Model",
                   "Models//Eru_soldier_playermodel.fbx",
                   "Models//Physmodel_textured.fbx",
                   "Stand_Animation_Pistol_Up_Idle_New",
                   "Stand_Animation_Pistol_Up_Idle_New",
                   "Stand_Animation_Rifle_Up", 
                   "Stand_Animation_Rifle_Up",
                   "Stand_Animation_Rifle_Up",
                   "Stand_Animation_Rifle_Up",
                   "Stand_Animation_Rifle_Up"
                   ));
        }        

        public void Create_GunLoadinfoAndProperties(GameSegment seg)
        {
            //please no duplicates
            /*
            seg.gunLoadInfos.Add(
                new GunLoadInfo(   
                    "hands", "Models//Hands_View.fbx", "Models//Pistol_modern_world_equip.fbx", "Hands_View_Idle",
                    "Hands_View_Shoot", "Pistol_Modern_Reload", "Pistol_Modern_Equip", "Pistol_Modern_Drop", "Noone",
                    "Sound_Pistol_Modern_Shoot", "Sound_Pistol_Modern_Reload", "Sound_Pistol_Modern_Equip", "Sound_Pistol_Modern_Drop",
                    "Sound_Pistol_Modern_Chamber"));
            */
            seg.gunLoadInfos.Add(
                new GunLoadInfo(
                    "pistol_modern", "Models//Pistol_modern_view.fbx", "Models//Pistol_modern_world_equip.fbx", "Pistol_View_Idle01",
                    "Pistol_View_Shoot01", "Pistol_Modern_Reload", "Pistol_Modern_Equip", "Pistol_Modern_Drop", "Pistol_Modern_Chamber",
                    "Sound_Pistol_Modern_Shoot", "Sound_Pistol_Modern_Reload", "Sound_Pistol_Modern_Equip", "Sound_Pistol_Modern_Drop",
                    "Sound_Pistol_Modern_Chamber"));
            

            //seg.gunproperties.Add(new Firearm_Property(10, 180, 1, 12, 1, AmmoTypes.type_9mm, new string[] { "noWeapon" }));
            seg.gunproperties.Add(new Firearm_Property(10, 180, 1, 12, 1, AmmoTypes.type_9mm, new string[] { "semi" }));           
        
        }

        public void Load_PlayerModelData(GameSegment seg, GeneralGameData dat)
        {
            FileStream s;
            for (int i = 0; i < seg.playerLoadInfos.Count; i++)
            {
                AnimatedModelMesh mesh = Animation_Functions.LoadAnimatedMeshFromFBX(dat, seg.playerLoadInfos[i].worldModel_filename);
                AnimatedModelMesh hitbox = Animation_Functions.LoadAnimatedMeshFromFBX(dat, seg.playerLoadInfos[i].hitboxModel_filename);
                
                //load textures
                s = new FileStream(dat.GameDirectory + mesh.relTextureNames[0], FileMode.Open);
                mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s);
                s.Close();

                seg.playerModels.Add(mesh);                    
                seg.hitboxModels.Add(hitbox);
            }
        }

        public void Load_WeaponModels(GameSegment seg, GeneralGameData dat, GraphicsDevice g)
        {
            FileStream s;
            for (int i = 0; i < seg.gunLoadInfos.Count; i++)
            {
                Generic_Firearm genericGunInfo = new Generic_Firearm();

                AnimatedModelMesh model_world, model_view; 

                //load mesh
                model_world = Animation_Functions.LoadAnimatedMeshFromFBX(dat, seg.gunLoadInfos[i].worldModel_filename);
                model_view = Animation_Functions.LoadAnimatedMeshFromFBX(dat, seg.gunLoadInfos[i].viewModel_filename);

                //load textures
                s = new FileStream(dat.GameDirectory + model_world.relTextureNames[0], FileMode.Open);
                model_world.DiffuseTexture = Texture2D.FromStream(g, s);
                s.Close();

                s = new FileStream(dat.GameDirectory + model_view.relTextureNames[0], FileMode.Open);
                model_view.DiffuseTexture = Texture2D.FromStream(g, s);
                s.Close();
                s = null;

                //Add to list
                seg.weaponModels_World.Add(model_world);
                seg.weaponModels_View.Add(model_view);

                genericGunInfo.Set_GenericFirearmData(seg.gunLoadInfos[i]);
                genericGunInfo.SetGunProperties(seg.gunproperties[i]);

                genericGunInfo.worldModelIndex = i;
                genericGunInfo.viewModelIndex = i;

                seg.generic_firearms.Add(genericGunInfo);
            }
        }        

        public void LoadSoundsToCollection(GameSegment segment, GeneralGameData gamedat)
        {
            segment.Sound_Collection.Add(new Sound_Container(gamedat, "pistol_sounds", new string[] { "//Sounds//Gun1_close.wav", "//Sounds//Shot01_distance06.wav", "//Sounds//Gun1_confined_medium.wav", "//Sounds//siren01.wav" }, 1));
        }

        public void Update_WeaponAndAnimation_World(GameSegment seg, GeneralGameData gamedat, int weaponModelChoice)
        {
            return;
            for (int i = 0; i < seg.Players.Count; i++)
            {
                if (seg.Players[i].WorldModel_AnimController.PlayAnimationOnce)
                {
                    seg.Players[i].WorldModel_AnimController.AnimationTimer++;

                    Animation_Functions.PlayAnimation_AnimColl(seg.shared_animation_human,
                        seg.Players[i].WorldModel_AnimController.currentAnimationName,
                       seg.weaponModels_World[0],
                       seg.Players[i].WorldModel_AnimController,
                       Matrix.CreateRotationY(-seg.Players[i].Get_PlayerRotation().Y + MathHelper.Pi) * Matrix.CreateTranslation(seg.Players[i].Get_PlayerPosition() + new Vector3(0, 4f, 0)),
                        "pasted__spine1", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(seg.Players[i].LeftRightBend_adapt.Z / 1.5f),
                        "Arm_right", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                        "Arm_left", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                        null);
                }
                //This plays the idle Animation if noone is played else
                else if (seg.Players[i].WorldModel_AnimController.PlayAnimationOnce == false)
                {
                    Animation_Functions.PlayAnimation_AnimColl(seg.shared_animation_human,
                       seg.Players[i].WorldModel_AnimController.idleAnimName,
                       seg.weaponModels_World[0],
                       seg.Players[i].WorldModel_AnimController,
                       Matrix.CreateRotationY(-seg.Players[i].Get_PlayerRotation().Y + MathHelper.Pi) * Matrix.CreateTranslation(seg.Players[i].Get_PlayerPosition() + new Vector3(0, 0.4f, 0)),
                       "pasted__spine1", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(seg.Players[i].LeftRightBend_adapt.Z / 1.5f),
                        "Arm_right", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                        "Arm_left", Matrix.CreateRotationX(seg.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                        null);

                    seg.Players[i].WorldModel_AnimController.AnimationTimer++;
                }
            }
        }
                
        public void Load_SharedAnimations_Packets(GameSegment seg, GeneralGameData gamedat)
        {
            seg.shared_animation_human = new AnimationContainer();
            seg.shared_animation_guns = new AnimationContainer();

            seg.shared_animation_human = Animation_Functions.LoadAnimationsFromFile(gamedat, "Animation//Animation_human_shared.txt", 1);
            //Load weapon animations here
            seg.shared_animation_guns = Animation_Functions.LoadAnimationsFromFile(gamedat, "Animation//Animation_weapon_shared.txt", 1);
        }

        public void LoadItemsForMap(GameSegment seg, GeneralGameData dat)
        {
            //Load All models Anyway
            StaticModelMesh health_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Health_Cross.fbx");
            StaticModelMesh armor_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Body_Armor.fbx");

            StaticModelMesh xray_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Xray_Item.fbx");
            StaticModelMesh dhealth_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Double_Health_Item.fbx");
            StaticModelMesh djump_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//DoubleJump_Item.fbx");
            StaticModelMesh dshot_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//DoubleShot_Powerup.fbx");
            StaticModelMesh gravity_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Gravity_Powerup.fbx");

            StaticModelMesh jetpack_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Jetpack_item.fbx");
            StaticModelMesh speed_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Speed_Powerup.fbx");
            StaticModelMesh teleport_mesh = ModelLoader.LoadMeshFromFBX(dat, "Models//Teleport_Powerup.fbx");

            #region get textures

            FileStream s1 = new FileStream(dat.GameDirectory + health_mesh.reltextureNames[0], FileMode.Open);
            health_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + armor_mesh.reltextureNames[0], FileMode.Open);
            armor_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + xray_mesh.reltextureNames[0], FileMode.Open);
            xray_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + dhealth_mesh.reltextureNames[0], FileMode.Open);
            dhealth_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + djump_mesh.reltextureNames[0], FileMode.Open);
            djump_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();
            //done

            s1 = new FileStream(dat.GameDirectory + dshot_mesh.reltextureNames[0], FileMode.Open);
            dshot_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + gravity_mesh.reltextureNames[0], FileMode.Open);
            gravity_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();
            //done
            s1 = new FileStream(dat.GameDirectory + jetpack_mesh.reltextureNames[0], FileMode.Open);
            jetpack_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + speed_mesh.reltextureNames[0], FileMode.Open);
            speed_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            s1 = new FileStream(dat.GameDirectory + teleport_mesh.reltextureNames[0], FileMode.Open);
            teleport_mesh.DiffuseTexture = Texture2D.FromStream(GraphicsDevice, s1);
            s1.Close();

            #endregion

            HealthPack pack = new HealthPack(new Vector3(-264, 25, -222), 100, 80 * 2, new Vector3(0, 0.015f, 0), health_mesh, new SphereConvexHull(Vector3.Zero, 2));
            BodyArmor armor = new BodyArmor(new Vector3(-275, 25, -230), 100, 80 * 60, new Vector3(0, 0.015f, 0), armor_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup djump_powerup = new Powerup(new Vector3(-280, 26, -248), 2.5f, "jump", 80 * 60, new Vector3(0, 0.015f, 0), djump_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup jetpack_powerup = new Powerup(new Vector3(-290, 26, -210), 2.5f, "jetpack", 80 * 60, new Vector3(0, 0.015f, 0), jetpack_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup speed_powerup = new Powerup(new Vector3(-300, 26, -190), 2.0f, "speed", 80 * 60, new Vector3(0, 0.015f, 0), speed_mesh, new SphereConvexHull(Vector3.Zero, 2));

            HealthPack pack2 = new HealthPack(new Vector3(-284, 39, 120), 100, 80 * 2, new Vector3(0, 0.015f, 0), health_mesh, new SphereConvexHull(Vector3.Zero, 2));
            BodyArmor armor2 = new BodyArmor(new Vector3(-285, 39, 115), 100, 80 * 60, new Vector3(0, 0.015f, 0), armor_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup djump_powerup2 = new Powerup(new Vector3(-280, 39, 108), 2.5f, "jump", 80 * 60, new Vector3(0, 0.015f, 0), djump_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup jetpack_powerup2 = new Powerup(new Vector3(-290, 39, 102), 2.5f, "jetpack", 80 * 60, new Vector3(0, 0.015f, 0), jetpack_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup speed_powerup2 = new Powerup(new Vector3(-300, 39, 98), 2.0f, "speed", 80 * 60, new Vector3(0, 0.015f, 0), speed_mesh, new SphereConvexHull(Vector3.Zero, 2));


            HealthPack pack3 = new HealthPack(new Vector3(244, 25, 250), 100, 80 * 2, new Vector3(0, 0.015f, 0), health_mesh, new SphereConvexHull(Vector3.Zero, 2));
            BodyArmor armor3 = new BodyArmor(new Vector3(230, 25, 260), 100, 80 * 60, new Vector3(0, 0.015f, 0), armor_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup djump_powerup3 = new Powerup(new Vector3(220, 26, 270), 2.5f, "jump", 80 * 60, new Vector3(0, 0.015f, 0), djump_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup jetpack_powerup3 = new Powerup(new Vector3(220, 26, 280), 2.5f, "jetpack", 80 * 60, new Vector3(0, 0.015f, 0), jetpack_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup speed_powerup3 = new Powerup(new Vector3(250, 26, 290), 2.0f, "speed", 80 * 60, new Vector3(0, 0.015f, 0), speed_mesh, new SphereConvexHull(Vector3.Zero, 2));

            HealthPack pack4 = new HealthPack(new Vector3(250, 25, -220), 100, 80 * 2, new Vector3(0, 0.015f, 0), health_mesh, new SphereConvexHull(Vector3.Zero, 2));
            BodyArmor armor4 = new BodyArmor(new Vector3(230, 25, -240), 100, 80 * 60, new Vector3(0, 0.015f, 0), armor_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup djump_powerup4 = new Powerup(new Vector3(220, 26, -270), 2.5f, "jump", 80 * 60, new Vector3(0, 0.015f, 0), djump_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup jetpack_powerup4 = new Powerup(new Vector3(220, 26, -280), 2.5f, "jetpack", 80 * 60, new Vector3(0, 0.015f, 0), jetpack_mesh, new SphereConvexHull(Vector3.Zero, 2));
            Powerup speed_powerup4 = new Powerup(new Vector3(250, 26, -290), 2.0f, "speed", 80 * 60, new Vector3(0, 0.015f, 0), speed_mesh, new SphereConvexHull(Vector3.Zero, 2));


            //Powerup xray_powerup = new Powerup(new Vector3(0, 26, -10), 1,"xray", 80 * 60, new Vector3(0, 0.015f, 0), xray_mesh, new SphereCollider(Vector3.Zero, 2));

            //Powerup dhealth_powerup = new Powerup(new Vector3(0, 26, -20), 2.0f, "health", 80 * 60, new Vector3(0, 0.015f, 0), dhealth_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup djump_powerup2 = new Powerup(new Vector3(0, 26, -30), 2.5f, "jump", 80 * 60, new Vector3(0, 0.015f, 0), djump_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup dshot_powerup = new Powerup(new Vector3(0, 26, -40), 2.0f, "shotfactor", 80 * 60, new Vector3(0, 0.015f, 0), dshot_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup gravity_powerup = new Powerup(new Vector3(0, 26, -50), 1, "gravity", 80 * 60, new Vector3(0, 0.015f, 0), gravity_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup jetpack_powerup2 = new Powerup(new Vector3(0, 26, -60), 2.5f, "jetpack", 80 * 60, new Vector3(0, 0.015f, 0), jetpack_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup speed_powerup2 = new Powerup(new Vector3(0, 26, -70), 2.0f, "speed", 80 * 60, new Vector3(0, 0.015f, 0), speed_mesh, new SphereCollider(Vector3.Zero, 2));
            //Powerup teleport_powerup = new Powerup(new Vector3(0, 26, -80), 2.5f, "teleport", 80 * 60, new Vector3(0, 0.015f, 0), teleport_mesh, new SphereCollider(Vector3.Zero, 2));

            seg.itemsInMap.Add(pack);
            seg.itemsInMap.Add(armor);
            seg.itemsInMap.Add(jetpack_powerup);
            seg.itemsInMap.Add(speed_powerup);
            seg.itemsInMap.Add(djump_powerup);

            seg.itemsInMap.Add(pack2);
            seg.itemsInMap.Add(armor2);
            seg.itemsInMap.Add(jetpack_powerup2);
            seg.itemsInMap.Add(speed_powerup2);
            seg.itemsInMap.Add(djump_powerup2);

            seg.itemsInMap.Add(pack3);
            seg.itemsInMap.Add(armor3);
            seg.itemsInMap.Add(jetpack_powerup3);
            seg.itemsInMap.Add(speed_powerup3);
            seg.itemsInMap.Add(djump_powerup3);

            seg.itemsInMap.Add(pack4);
            seg.itemsInMap.Add(armor4);
            seg.itemsInMap.Add(jetpack_powerup4);
            seg.itemsInMap.Add(speed_powerup4);
            seg.itemsInMap.Add(djump_powerup4);


            //Find all textures for the loaded models
            for (int i = 0; i < seg.itemsInMap.Count; i++)
            {
            }
        }

        public void LoadNpcNodeMesh(GameSegment seg, GeneralGameData dat)
        {
            MapLoaderNew loader = new MapLoaderNew();
            MeshGroup mesh = loader.getMeshGroups(dat.GameDirectory, "Npc_Path_Node_Model")[0];
            mesh.TextureDiffuse = Texture2D.FromStream(this.GraphicsDevice, new FileStream(dat.GameDirectory + "//Textures//" + mesh.material.texturePath, FileMode.Open));
            //seg.Npc_Path_collection.NodeMesh = mesh;

        }

        public void UpdateFunctionCounters(GeneralGameData gamedat)
        {
            for (int i = 0; i < 4; i++)
            {
                if (gamedat.function_counters[i] >= MathHelper.TwoPi)
                    gamedat.function_counters[i] = 0.0f;

                gamedat.function_counters[i] += 0.010f;
            }
        }
        
        public void ApplyGraphicsData(GeneralGameData gameData, GraphicsDeviceManager GraphicsDeviceManager)
        {
            if (gameData.Framerate == 0)
                this.IsFixedTimeStep = false;
            else
                this.IsFixedTimeStep = true;

            this.TargetElapsedTime = TimeSpan.FromSeconds(1f / (float)gameData.Framerate);

            Window.AllowUserResizing = gameData.AllowUserResizing;
            GraphicsDeviceManager.PreferredBackBufferHeight = gameData.height;
            GraphicsDeviceManager.PreferredBackBufferWidth = gameData.width;
            GraphicsDeviceManager.IsFullScreen = gameData.fullscreen;
            GraphicsDeviceManager.PreferMultiSampling = gameData.MultiSampling;
            RasterizerState raster = new RasterizerState();
            //raster.FillMode = FillMode.WireFrame;
            raster.MultiSampleAntiAlias = true;
            GraphicsDevice.RasterizerState = raster;
            GraphicsDeviceManager.ApplyChanges();
        }

        public void LoadMapMeshes_OLD_loadsintoRam(String GamePath, String RelativePathAndFileName, GameSegment segment)
        {
            segment.Level.MapMeshes = segment.Level.maploader.getMeshGroups(GamePath, RelativePathAndFileName);
            FileStream t, b, s;
            long texturebytes = 0;

            for (int i = 0; i < segment.Level.MapMeshes.Count; i++)
            {
                try
                {
                    t = new FileStream(GamePath + "//textures//" + segment.Level.MapMeshes[i].material.texturePath, FileMode.Open);
                    segment.Level.MapMeshes[i].TextureDiffuse = Texture2D.FromStream(GraphicsDevice, t);
                    segment.Level.MapMeshes[i].Phong.DiffuseMap = segment.Level.MapMeshes[i].material.texturePath;
                    segment.Level.MapMeshes[i].Phong.BumpMap = segment.Level.MapMeshes[i].material.bumppath;
                    segment.Level.MapMeshes[i].Phong.SpecularMap = segment.Level.MapMeshes[i].material.specularpath;
                    texturebytes += segment.Level.MapMeshes[i].TextureDiffuse.Width * segment.Level.MapMeshes[i].TextureDiffuse.Height * 4;
                    t.Close();

                }
                catch { }
                try
                {
                    b = new FileStream(GamePath + "//textures//" + segment.Level.MapMeshes[i].material.bumppath, FileMode.Open);
                    segment.Level.MapMeshes[i].TextureBump = Texture2D.FromStream(GraphicsDevice, b);
                    b.Close();
                }
                catch
                {
                    segment.Level.MapMeshes[i].TextureBump = segment.normal_basic;
                }
                try
                {
                    s = new FileStream(GamePath + "//Textures//" + segment.Level.MapMeshes[i].material.specularpath, FileMode.Open);
                    segment.Level.MapMeshes[i].TextureSpecular = Texture2D.FromStream(GraphicsDevice, s);
                    s.Close();
                }
                catch
                {
                    segment.Level.MapMeshes[i].TextureSpecular = segment.specular_basic;
                }
                Console.WriteLine("{0} KB ", texturebytes / 1000);
            }
            long kb = texturebytes / 1000;
            long mb = texturebytes / 1000000;
            double gb = (double)texturebytes / (double)1000000000;
        }

        public void LoadMapMeshes_VB(String GamePath, String RelativePathAndFileName, GameSegment segment)
        {
            ObjGroupsAndMaterialsPack visualMap = OBJ_Loader.Open_OBJ(GamePath, RelativePathAndFileName);

            //Load the materials
            segment.Level.materials = new Obj_Material[visualMap.obj_mats.Length];
            FileStream texStream;

            for (int i = 0; i < visualMap.obj_mats.Length; i++)
            {
                segment.Level.materials[i] = visualMap.obj_mats[i];
                try
                {
                    texStream = new FileStream(gameData.GameDirectory + "//textures//" + visualMap.obj_mats[i].colormap_path, FileMode.Open);
                    segment.Level.materials[i].color = Texture2D.FromStream(GraphicsDevice, texStream);
                    texStream.Close();
                }
                catch { }
                try
                {
                    texStream = new FileStream(gameData.GameDirectory + "//textures//" + visualMap.obj_mats[i].bumpmap_path, FileMode.Open);
                    segment.Level.materials[i].bump = Texture2D.FromStream(GraphicsDevice, texStream);
                    texStream.Close();
                }
                catch { }
                try
                {
                    texStream = new FileStream(gameData.GameDirectory + "//textures//" + visualMap.obj_mats[i].specularmap_path, FileMode.Open);
                    segment.Level.materials[i].specular = Texture2D.FromStream(GraphicsDevice, texStream);
                    texStream.Close();
                }
                catch { }
            }

            //Now create vb's
            for (int i = 0; i < visualMap.groups.Length; i++)
            {
                for (int j = 0; j < visualMap.groups[i].faces.Length; j++)
                {
                    visualMap.groups[i].faces[j].vertexbuffer = new VertexBuffer(GraphicsDevice, CustomVertex.vertexDeclaration, visualMap.groups[i].faces[j].vertexlist.Count, BufferUsage.WriteOnly);
                    visualMap.groups[i].faces[j].vertexbuffer.SetData(visualMap.groups[i].faces[j].vertexlist.ToArray());
                    visualMap.groups[i].faces[j].vertexlist = null;

                }
            }
            segment.Level.visualMap = visualMap.groups;
            segment.Level.materials = visualMap.obj_mats;

            visualMap.obj_mats = null;
            visualMap.groups = null;
        }

        public void LoadMapBlocks(String GamePath, String RelativePathAndFileName, GameSegment segment)
        {
            this.Segment01.Level.MapBlocks = Segment01.Level.maploader.get_SplitMapMeshesAsBlock(GamePath, RelativePathAndFileName + "_phys");
        }

        public void InitialsieMirrors()
        {
            Mirror mir = new Mirror(new Vector3(100, 20, 30), Vector3.Zero, 1, MathHelper.PiOver2);
            mir.RenderTarget = new RenderTarget2D(GraphicsDevice, GraphicsDevice.PresentationParameters.BackBufferWidth, GraphicsDevice.PresentationParameters.BackBufferHeight, true, SurfaceFormat.Color, DepthFormat.None, 4, RenderTargetUsage.PlatformContents);
            Segment01.Mirrors.Add(mir);
        }

        public void LoadFlashLightTexture(string GamePath, String textureName, GameSegment segment)
        {
            segment.FlashLightTexture = Texture2D.FromStream(GraphicsDevice, new FileStream(GamePath + "//" + textureName, FileMode.Open));
        }

        public void InitialiseShadows()
        {
            Shadow shad = new Shadow(new Vector3(-60, 60, -20), Vector3.Zero, Vector3.Up, 1, MathHelper.PiOver2);
            shad.RenderTarget = new RenderTarget2D(GraphicsDevice, 2048, 2048, false, SurfaceFormat.Color, DepthFormat.Depth24);
            Segment01.Shadows.Add(shad);
        }

        public void GenerateLightMaps_FromScriptAndBlocks(GeneralGameData dat, GameSegment seg)
        {
            NumberFormatInfo info = CultureInfo.InvariantCulture.NumberFormat;

            Block[] lightblocks = Segment01.Level.maploader.get_SplitMapMeshesAsBlock(dat.GameDirectory, dat.RelativeMapFileAndPath + "_lights").ToArray();
            String[] lightScript = File.ReadAllLines(dat.GameDirectory + "//" + dat.RelativeMapFileAndPath + "_lightScripts.txt");
            List<String> LightScriptClean = new List<string>();
            List<Light> lights = new List<Light>();
            int LightsCount = 0;

            List<List<String>> Light_ListOfListStrings = new List<List<string>>();

            if (lightScript == null)
                return;

            for (int i = 0; i < lightScript.Length; i++)
            {
                if (lightScript[i].StartsWith("#"))
                    continue;

                if (lightScript[i].Contains("{"))
                {
                    Light_ListOfListStrings.Add(new List<string>());
                    continue;
                }

                LightScriptClean.Add(lightScript[i]);

            }
            lightScript = null;

            for (int i = 0; i < LightsCount; i++)
            {
                Light_ListOfListStrings.Add(new List<string>());
            }

            int currentIndex = 0;
            int counter = 0;
            while (counter < LightScriptClean.Count)
            {
                if (LightScriptClean[counter].Length > 3)
                    Light_ListOfListStrings[currentIndex].Add(LightScriptClean[counter].Trim(new char[] { '\t', '\n', '}', '{', ' ' }));

                if (LightScriptClean[counter].Contains("}"))
                {
                    currentIndex++;
                }
                counter++;
            }

            for (int i = 0; i < Light_ListOfListStrings.Count; i++)
            {
                int lighttype = -1;
                string name = string.Empty;
                float intensity = 0;
                byte falloff = 0;
                float distance = 0;
                Vector4 lightcolor = Vector4.Zero;
                Vector3 direction = Vector3.Zero;
                int resw = 0, resh = 0;
                float fieldofview = 0;

                for (int j = 0; j < Light_ListOfListStrings[i].Count; j++)
                {
                    if (Light_ListOfListStrings[i][j].StartsWith("Directed_Light "))
                    {
                        lighttype = 0; //Directed light
                        name = Light_ListOfListStrings[i][j].Substring(15);
                    }

                    if (Light_ListOfListStrings[i][j].StartsWith("Point_Light "))
                    {
                        lighttype = 1; // point light
                        name = Light_ListOfListStrings[i][j].Substring(12);
                    }

                    if (Light_ListOfListStrings[i][j].StartsWith("intensity "))
                        intensity = Convert.ToSingle(Light_ListOfListStrings[i][j].Substring(11), info);

                    if (Light_ListOfListStrings[i][j].StartsWith("falloff "))
                        falloff = Convert.ToByte(Light_ListOfListStrings[i][j].Substring(9), info);

                    if (Light_ListOfListStrings[i][j].StartsWith("distance "))
                        distance = Convert.ToSingle(Light_ListOfListStrings[i][j].Substring(11), info);

                    if (Light_ListOfListStrings[i][j].StartsWith("lightcolor "))
                    {
                        string[] splits = Light_ListOfListStrings[i][j].Substring(12).Split(',');
                        lightcolor = new Vector4(Convert.ToSingle(splits[0]), Convert.ToSingle(splits[1]), Convert.ToSingle(splits[2]), Convert.ToSingle(splits[3]));
                    }
                    if (Light_ListOfListStrings[i][j].StartsWith("direction "))
                    {
                        string[] splits = Light_ListOfListStrings[i][j].Substring(11).Split(',');
                        direction = new Vector3(Convert.ToSingle(splits[0]), Convert.ToSingle(splits[1]), Convert.ToSingle(splits[2]));
                    }
                    if (Light_ListOfListStrings[i][j].StartsWith("res_w "))
                        resw = Convert.ToInt32(Light_ListOfListStrings[i][j].Substring(7));

                    if (Light_ListOfListStrings[i][j].StartsWith("res_h "))
                        resh = Convert.ToInt32(Light_ListOfListStrings[i][j].Substring(7));

                    if (Light_ListOfListStrings[i][j].StartsWith("fieldofview "))
                        fieldofview = Convert.ToSingle(Light_ListOfListStrings[i][j].Substring(14), info);
                }
                Light li;
                if (lighttype == 0)
                {
                    //Directed light

                    li = new Light(name, lighttype, intensity, falloff, distance, lightcolor, Vector3.Zero, direction, resw, resh, fieldofview / (360 / MathHelper.TwoPi));
                    li.Position = (lightblocks[i].wallModel.CenterPosition + lightblocks[i].floorModel.CenterPosition) / 2;
                    
                    li.lightmap = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.viewDirected = Matrix.CreateLookAt(li.Position, direction + li.Position, Vector3.Forward);

                    lights.Add(li);
                }
                else
                {
                    // point light
                    li = new Light(name, lighttype, intensity, falloff, distance, lightcolor, Vector3.Zero, Vector3.Zero, resw, resh, MathHelper.PiOver2);
                    li.Position = (lightblocks[i].wallModel.CenterPosition + lightblocks[i].floorModel.CenterPosition) / 2;

                    li.top = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.left = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.right = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.front = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.back = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);
                    li.bottom = new RenderTarget2D(GraphicsDevice, li.res_x, li.res_y, false, SurfaceFormat.Vector4, DepthFormat.Depth16);

                    li.viewDirected = Matrix.CreateLookAt(li.Position, direction + li.Position, Vector3.Forward);

                    lights.Add(li);
                }
            }
            seg.Level.mapLights = lights.ToArray();
        }

        public void LoadSprite_Light(GeneralGameData gamedat, GameSegment segment)
        {
            segment.spriteContainer.SpriteTextures = new List<Texture2D>();
            FileStream s;
            s = new FileStream(gamedat.GameDirectory + "//Effects//bluelite.png", FileMode.Open);
            segment.spriteContainer.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();

            s = new FileStream(gamedat.GameDirectory + "//Effects//redlite.png", FileMode.Open);
            segment.spriteContainer.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();

            s = new FileStream(gamedat.GameDirectory + "//Effects//greenlite.png", FileMode.Open);
            segment.spriteContainer.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();
        }

        public void LoadSprite_GunFlash(GeneralGameData gamedat, GameSegment segment)
        {
            return;
            /*
            segment.firearmSprites.SpriteTextures = new List<Texture2D>();
            FileStream s;
            s = new FileStream(gamedat.GameDirectory + "//Effects//bluelite.png", FileMode.Open);
            segment.firearmSprites.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();

            s = new FileStream(gamedat.GameDirectory + "//Effects//redlite.png", FileMode.Open);
            segment.firearmSprites.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();

            s = new FileStream(gamedat.GameDirectory + "//Effects//greenlite.png", FileMode.Open);
            segment.firearmSprites.SpriteTextures.Add(Texture2D.FromStream(this.GraphicsDevice, s));
            s.Close();
            */
        }

        //This will merge the Levels Point Lights and the Sprite generated Pointlights into one List to give the shader
        //Also, it will determine which Lights are to be rendered and which not

        public void CreateTemporarySprites(GeneralGameData gamedat, GameSegment segment)
        {
            segment.spriteContainer.Sprites.Add(new EffectSprite(new Vector3(25, 0, 0), Vector3.Zero, 44055, 1.3f, 1, 1f, 0, new Vector4(0.5f, 1f, 0.5f, 1), new Vector4(0.5f, 1f, 0.5f, 1), Vector3.Zero, 0, segment.spriteContainer.SpriteTextures[2]));
            segment.spriteContainer.Sprites[0].Texture = segment.spriteContainer.SpriteTextures[2];
            segment.spriteContainer.Sprites.Add(new EffectSprite(new Vector3(30, 0, 30), Vector3.Zero, 244550, 1.3f, 1, 1f, 0, new Vector4(1.0f, 0.5f, 0.5f, 1), new Vector4(1.0f, 0.5f, 0.5f, 1), Vector3.Zero, 0, segment.spriteContainer.SpriteTextures[1]));
            segment.spriteContainer.Sprites[1].Texture = segment.spriteContainer.SpriteTextures[1];
            segment.spriteContainer.Sprites.Add(new EffectSprite(new Vector3(30, 0, -300), Vector3.Zero, 44350, 1.3f, 1, 1f, 0, new Vector4(0.5f, 0.5f, 1f, 1), new Vector4(0.5f, 0.5f, 1f, 1), Vector3.Zero, 0, segment.spriteContainer.SpriteTextures[2]));
            segment.spriteContainer.Sprites[2].Texture = segment.spriteContainer.SpriteTextures[2];
            segment.spriteContainer.Sprites.Add(new EffectSprite(new Vector3(0, 0, 0), Vector3.Zero, 45404, 1.4f, 11, 1f, 1, new Vector4(0.2f, 0.2f, 1.0f, 1), new Vector4(0.2f, 0.2f, 1.0f, 1), Vector3.Zero, 0, segment.spriteContainer.SpriteTextures[0]));
            segment.spriteContainer.Sprites[3].Texture = segment.spriteContainer.SpriteTextures[0];

        }

        //Load only one
        public void LoadVehicle(String GamePath, String RelativePathAndFileVisualMesh, String RelativePathAndFilePhysMesh, GameSegment segment)
        {
            segment.PhysicsModels = new List<PhysicsModel>();
            Vehicle VehicleObj = new Vehicle(100, 200, 1, new Vector3(50, 0, 0), new Vector3(0, 0, 0));
            VehicleObj.Speed = 0.2f;
            PhysicsModel physmodel = new PhysicsModel();
            physmodel.visualMesh = new MeshGroup();
            physmodel.physicsMesh = new MeshGroup();

            physmodel.visualMesh = segment.Level.maploader.getMeshGroups(GamePath, RelativePathAndFileVisualMesh)[0];
            physmodel.physicsMesh = segment.Level.maploader.getMeshGroups(GamePath, RelativePathAndFilePhysMesh)[0];

            FileStream t, b, s;
            try
            {
                t = new FileStream(GamePath + "//Textures//" + physmodel.visualMesh.material.texturePath, FileMode.Open);
                physmodel.visualMesh.TextureDiffuse = Texture2D.FromStream(GraphicsDevice, t);
                physmodel.visualMesh.Phong.DiffuseMap = physmodel.visualMesh.material.texturePath;
                physmodel.visualMesh.Phong.BumpMap = physmodel.visualMesh.material.bumppath;
                physmodel.visualMesh.Phong.SpecularMap = physmodel.visualMesh.material.specularpath;
                t.Close();
                t = null;

            }
            catch { }
            try
            {
                b = new FileStream(GamePath + "//Textures//" + physmodel.visualMesh.material.bumppath, FileMode.Open);
                physmodel.visualMesh.TextureBump = Texture2D.FromStream(GraphicsDevice, b);
                b.Close();
                b = null;

            }
            catch
            {
                physmodel.visualMesh.TextureBump = segment.normal_basic;
            }
            try
            {
                s = new FileStream(GamePath + "//Textures//" + physmodel.visualMesh.material.specularpath, FileMode.Open);
                physmodel.visualMesh.TextureSpecular = Texture2D.FromStream(GraphicsDevice, s);
                s.Close();
                s = null;
            }
            catch
            {
                physmodel.visualMesh.TextureSpecular = segment.specular_basic;
            }
            VehicleObj.CalculateWheelPositions(
                new Vector3[4]{
                    new Vector3(3, 0.0f, 3.7f), 
                    new Vector3(-3, 0.0f, 3.7f), 
                    new Vector3(3, 0.0f, -4.7f), 
                    new Vector3(-3, 0.0f, -4.7f)
                });

            VehicleObj.physicsModel = physmodel;

            segment.Vehicles.Add(VehicleObj);
        }

        public void CreateNpcs(GameSegment seg)
        {
            for (int i = 0; i < 5; i++)
            {
                NPC_Follower generic = new NPC_Follower(
                    this.GraphicsDevice,
                    "NPC BOB",
                    gameData,
                    ref seg,
                    ref seg.shared_animation_human,
                    "Stand_Animation_Casual_Breathing",
                    "Models//Physmodel_textured.fbx",
                    "Models//Physmodel_textured.fbx",
                    new Vector3(-90, 10, -44),
                    new Vector3(0, 0, 0),
                    0.120f,
                    3,
                    1,
                    3.5f,
                    30); //node radius                
                generic.sphereConvexHull = new SphereConvexHull(new Vector3(generic.Position.X, generic.Position.Y + 1.3f, generic.Position.Z), 1.3f);

                generic.pathRoute = new NPC_PathFindRoute();
                seg.npc_level.Npcs.Add(generic);
            }
        }

        public void Load_NPC_NodesAndMesh(GameSegment seg, GeneralGameData dat)
        {
            FileStream s = new FileStream(dat.GameDirectory + "//textures//node_tex.jpg", FileMode.Open);
            seg.npc_level.line_texture = Texture2D.FromStream(GraphicsDevice, s);
            s.Close();
            seg.npc_level.Npc_Node_Collection_list.Add(NPC_Functions.Load_NPC_Nodes(0, "Walk01", "walking", "_NPC_Walk_Nodes_01", dat, seg));

            seg.npc_level.npc_node_mesh = Segment01.Level.maploader.get_SplitMapMeshesAsBlock(dat.GameDirectory, "//Models//NPC_Node")[0];
            seg.npc_level.Npc_Node_Collection_list[0].nodeBlock = NPC_Functions.Load_NPC_Nodes_BlocksMesh("_NPC_Walk_Nodes_01", dat, seg);
            if (seg.npc_level.Npc_Node_Collection_list.Count > 1)
                throw new Exception();

            ImportData(seg, dat, 0, "NPC_TestGround_NPC_Walk_Nodes_01_nodeInfo.txt");

        }

        public void Handle_NpcNode_Select(GameSegment seg, GeneralGameData dat)
        {
            seg.npc_level.input = Microsoft.Xna.Framework.Input.Mouse.GetState();


            bool intersect = false;
            float distance = 50000, currentDis = 0; //We only want one selected each frame!
            int Index = -1;

            //----------------------------------------------------------------------------------//
            //Step 1: FIRST IDENTIFY A NODE 
            //----------------------------------------------------------------------------------//
            #region
            //Since node List is the same order as the blocks list, logically...
            for (int i = 0; i < seg.npc_level.Npc_Node_Collection_list[0].nodeBlock.Count; i++)
            {

                //We go throught each block, and check for ray/triangle intersect with the players look direction
                Ray r = new Ray(seg.Players[0].PlayerCamera.Get_Positon(), seg.Players[0].PlayerCamera.CameraLookForwardDirection);


                seg.npc_level.Npc_Node_Collection_list[0].nodes[i].selected = 0;

                //Now go throught each triangle list!
                for (int j = 0; j < seg.npc_level.Npc_Node_Collection_list[0].nodeBlock[i].floorModel.vertices.Length; j += 3)
                {
                    intersect = CollisionDetection.checkTriangleIntersect(
                        seg.npc_level.Npc_Node_Collection_list[0].nodeBlock[i].floorModel.vertices[j].Position,
                        seg.npc_level.Npc_Node_Collection_list[0].nodeBlock[i].floorModel.vertices[j + 1].Position,
                        seg.npc_level.Npc_Node_Collection_list[0].nodeBlock[i].floorModel.vertices[j + 2].Position,
                        r);

                    if (intersect)
                    {
                        currentDis = VectorMath.get_length(
                                Vector3.Subtract(seg.Players[0].PlayerCamera.Get_Positon(),
                                seg.npc_level.Npc_Node_Collection_list[0].nodes[i].Position));
                        if (currentDis < distance)
                        {
                            distance = currentDis;
                            Index = i;
                        }
                    }
                }
            }
            #endregion

            //Our node right now will be "smallestIndex"
            if (Index >= 0)
                seg.npc_level.Npc_Node_Collection_list[0].nodes[Index].selected = 1;

            //----------------------------------------------------------------------------------//
            //Step 2: We need to give the index to the other nodes neighbourlist
            //----------------------------------------------------------------------------------//

            #region
            //First step, we need to store the active and passive selected nodes when left button is clicked
            //We do that over NPC_Level
            if (Index >= 0 && (seg.npc_level.input.LeftButton == ButtonState.Pressed))
            {
                seg.npc_level.activeNode = Index;
                Console.WriteLine("Active Index set to {0}", Index);
            }

            if (Index >= 0 && (seg.npc_level.input.RightButton == ButtonState.Pressed))
            {
                seg.npc_level.passiveNode = Index;
                Console.WriteLine("passive Index set to {0}", Index);
                //Now we have 2 indices

                //check if its added already part of the neighbour nodes-list

                //Important here: Active node gets the index of passive as a neighbour!
                bool containsPassiveAsNeighbour = seg.npc_level.Npc_Node_Collection_list[0].nodes[seg.npc_level.activeNode].neighbourNodes.Contains(seg.npc_level.passiveNode);
                if (!containsPassiveAsNeighbour)
                {
                    //Add the passive-node-index to the active-node-neighbour-list
                    seg.npc_level.Npc_Node_Collection_list[0].nodes[seg.npc_level.activeNode].neighbourNodes.Add(seg.npc_level.passiveNode);
                    Console.WriteLine("Active node {0} now added the passive node {1} to its neighbours", seg.npc_level.activeNode, seg.npc_level.passiveNode);
                }

                //Now we need to see, if the passive node already contains the active node as an Index, if not, add it of course
                bool passiveContainsActiveAsNeighbour = seg.npc_level.Npc_Node_Collection_list[0].nodes[seg.npc_level.passiveNode].neighbourNodes.Contains(seg.npc_level.activeNode);
                if (!passiveContainsActiveAsNeighbour)
                {
                    //Add the active-node-index to the pastive-node-neighbour-list
                    seg.npc_level.Npc_Node_Collection_list[0].nodes[seg.npc_level.passiveNode].neighbourNodes.Add(seg.npc_level.activeNode);
                    Console.WriteLine("passive node {0} now also added the active node {1} to its neighbours", seg.npc_level.passiveNode, seg.npc_level.activeNode);

                }

                //At the end, reset those, so they cant mess up something                
                Index = -1;
                //seg.npc_level.activeNode = -1;
                //seg.npc_level.passiveNode = -1;
            }

            #endregion


            if (seg.npc_level.input.MiddleButton == ButtonState.Pressed)
            {
                if (seg.npc_level.export_node_timer == 0)
                {
                    ExportNodeData(seg, dat, 0, "NPC_TestGround_NPC_Walk_Nodes_01_nodeInfo.txt");
                    seg.npc_level.export_node_timer = 1000;
                }
            }
            if (seg.npc_level.export_node_timer > 0)
                seg.npc_level.export_node_timer--;
        }

        public void UpdateNPC_Behaviour_and_Animation(GameSegment seg, GeneralGameData dat)
        {
            NPC_BehaviourFunctions.Animation_Behaviour(0, seg);
            NPC_BehaviourFunctions.Decide_NPC_ActMode(0, seg, dat, seg.Players[0]);
            NPC_BehaviourFunctions.ActOnSight(0, seg, 1.0f, seg.Players[0], dat);
            NPC_BehaviourFunctions.WalkAlongPath(0, seg, 1.0f, dat);
            NPC_BehaviourFunctions.HandleNpcCollision(0, seg);
            NPC_BehaviourFunctions.updateNPC(0, seg);
            NPC_BehaviourFunctions.tempAnimationController(seg);

        }

        public void Update_FirearmSprites(GameSegment seg, GeneralGameData dat)
        {
            /*
            for(int i =0; i < seg.Players.Count; i++)
            {
                if (seg.firearmSprites.Sprites[i] != null)
                {
                    Vector3 p = seg.Players[i].Position_Player;
                    Vector3 spritep = seg.Players[i].Position_Player + new Vector3(0, 10, 0);

                    Matrix m = Matrix.CreateRotationY(seg.Players[i].PlayerCamera.Get_Rotation().Y);
                    var newspritepos = Vector3.Transform(spritep, m);

                    seg.firearmSprites.Sprites[i].UpdateSprite(p, newspritepos, 1);
                    
                    if (seg.firearmSprites.Sprites[i].SpriteTimer == 0)
                    {
                        seg.firearmSprites.Sprites[i] = null;
                    }
                }
            }
            */
        }

        //Handle the Idle/Play Once animation playback, including the Matrices that add in the hierarchy for any selected joints
        //Also updates Hitbox Model

        public void Update_PlayerAnimationPlaybackAndEvents(GameSegment seg, GeneralGameData dat)
        {
            if (seg.Players.Count < 1)
                return;

            Microsoft.Xna.Framework.Input.KeyboardState kk = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (kk.IsKeyDown(Keys.D1))
            {
                seg.Level.draw_normal = true;
                seg.Level.draw_vb = false;
            }
            if (kk.IsKeyDown(Keys.D2))
            {
                seg.Level.draw_vb = true;
                seg.Level.draw_normal = false;
            }

            #region Animation Playback Control
            for (int i = 0; i < seg.Players.Count; i++)
            {
                #region Handle SHooting and sound playback //MOVE THIS LATER

                if (seg.Players[0].firearm_current.ShootDelayTimer > 0)
                    seg.Players[0].firearm_current.ShootDelayTimer--;

                if (kk.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.F) && i == 0)
                {
                    if (seg.Players[i].ViewGun_AnimController.PlayAnimationOnce == false)
                    {
                        seg.Sound_Collection[0].PlaySound(0, Vector3.Zero, Vector3.Zero, Vector3.Zero, 0.5f);
                    
                        seg.Players[i].firearm_current.ShootDelayTimer = 10;

                        AnimationController.setAnimationPlayback_Once(
                                  seg.Players[i].ViewGun_AnimController,
                                seg.Players[i].firearm_current.shootAnimation);
                        
                        Shooting_Data d = new Shooting_Data();
                        d.shoot_ray = new Ray(seg.Players[0].PlayerCamera.CameraRelativePosition_Adapt + seg.Players[0].Position_Player, seg.Players[0].PlayerCamera.CameraLookForwardDirection);
                        d.shot_power = 1;
                        d.bullet_type = 0;

                        //Problem , each model must be updated  at draw time.
                        //seg.npc_level.Npcs[0].get_Shot(d, seg.npc_level.Npcs[0].Hitbox_model, mat_hitbox);
                    }
                }
                #endregion
            }
        }

        public void ExportNodeData(GameSegment seg, GeneralGameData dat, int nodeCollectionIndex, string fileName)
        {
            string Nodes_as_text = string.Empty;

            //THis list will be very simple, since its based on the nodes .obj file too
            //Each node is given as its Index with a list of indices following
            //The Top contains 
            //"NameOfNodeCollection"
            //"relatedFileName" 
            //"NodeCount" 
            //"NodeCollectionType"
            // Nodes: 
            // nodeIndex: nb,nb,nb,nb;
            // 0: 3,2,5,1;  <-- Like that

            Nodes_as_text += seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].NodeCollection_Name + "\r\n";
            Nodes_as_text += fileName + "\r\n";
            Nodes_as_text += Convert.ToString(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes.Count) + "\r\n";
            Nodes_as_text += seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].NodeColleciton_Type + "\r\n";
            Nodes_as_text += "Nodes:" + "\r\n";

            for (int i = 0; i < seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes.Count; i++)
            {
                Nodes_as_text += Convert.ToString(i) + ":";

                for (int j = 0; j < seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].neighbourNodes.Count; j++)
                {
                    Nodes_as_text += Convert.ToString(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].neighbourNodes[j]);

                    if (j < seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[i].neighbourNodes.Count - 1)
                        Nodes_as_text += ",";
                }
                Nodes_as_text += ";\r\n";
            }

            Console.WriteLine("Writing the file");

            using (StreamWriter writer = new StreamWriter(dat.GameDirectory + "//" + fileName))
            {
                writer.Write(Nodes_as_text);
                writer.Close();
            }
        }

        public void ImportData(GameSegment seg, GeneralGameData dat, int nodeCollectionIndex, string fileName)
        {
            string[] NodeLines;

            try
            {
                NodeLines = File.ReadAllLines(dat.GameDirectory + "//" + fileName);
            }
            catch
            {
                return;
            }
            if (NodeLines == null)
                return;

            seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].NodeCollection_Name = NodeLines[0];
            int count = Convert.ToInt32(seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].NodeColleciton_Type = NodeLines[2]);
            seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].NodeColleciton_Type = NodeLines[3];

            for (int i = 5, j = 0; j < count; i++, j++)
            {
                string[] split = NodeLines[i].Trim(';').Split(':');
                split = split[1].Split(',');
                seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[j].neighbourNodes.Clear();
                for (int x = 0; x < split.Length; x++)
                {
                    if (split[x].Length > 0)
                        seg.npc_level.Npc_Node_Collection_list[nodeCollectionIndex].nodes[j].neighbourNodes.Add(Convert.ToInt32(split[x]));
                }
            }

            return;
        }

        public void LoadLineShader(GameSegment seg, GeneralGameData dat)
        {
            seg.npc_level.lineEffect = Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//VertexColorShader");
            seg.npc_level.pathLines = new List<NPC_PathLines>();
            seg.npc_level.pathLines.Add(new NPC_PathLines(0, "Walk01"));
            seg.npc_level.pathLines.Add(new NPC_PathLines(1, "Walk02"));

            FileStream s = new FileStream(dat.GameDirectory + "//textures//line_texture.png", FileMode.Open);
            seg.npc_level.line_texture = Texture2D.FromStream(GraphicsDevice, s);
            s.Close();

            return;
        }

        public void LoadSkyBox(string GamePath, String RelativePathAndFileName, GameSegment segment)
        {
            SkyBox skybox = new SkyBox();
            skybox.Mesh = segment.Level.maploader.getMeshGroups(GamePath, RelativePathAndFileName)[0];
            skybox.Shader = new Phong();

            skybox.Shader.DiffuseMap = skybox.Mesh.material.texturePath; //Absolute path

            FileStream s = new FileStream(GamePath + "//textures//" + skybox.Mesh.material.texturePath, FileMode.Open);

            skybox.Mesh.TextureDiffuse = Texture2D.FromStream(GraphicsDevice, s);

            segment.Level.MapSkyboxes.Add(skybox);
            s.Close();
            s = null;
        }

        public void Load_Shaders(GameSegment segment)
        {
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//Phong"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//ShadowShader"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//AnimatedModel"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//Blinn"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//DrawModel"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//BLINN_0"));

            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//ShadowShader"));
            segment.Effects.Add(Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//DrawWithShadow"));
        }

        public void CreateViewports(GeneralGameData gengamedat, GameSegment segment)
        {
            Console.WriteLine("Creating viewPort");
            if (gengamedat.PlayerCount == 1)
            {
                segment.viewports[0] = new Viewport(0, 0, gengamedat.width, gengamedat.height);
            }

            if (gengamedat.PlayerCount == 2)
            {
                segment.viewports[0] = new Viewport(0, 0, gengamedat.width, gengamedat.height / 2);
                segment.viewports[1] = new Viewport(0, gengamedat.height / 2, gengamedat.width, gengamedat.height / 2);

            }

            if (gengamedat.PlayerCount == 3)
            {
                segment.viewports[0] = new Viewport(0, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[1] = new Viewport(gengamedat.width / 2, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[2] = new Viewport(0, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[3] = new Viewport(gengamedat.width / 2, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
            }

            if (gengamedat.PlayerCount == 4 && gameData.wideFormat)
            {
                segment.viewports[0] = new Viewport(0, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[1] = new Viewport(gengamedat.width / 2, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[2] = new Viewport(0, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[3] = new Viewport(gengamedat.width / 2, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
            }
            if (gengamedat.PlayerCount == 4 && gameData.wideFormat == false)
            {
                segment.viewports[0] = new Viewport(0, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[1] = new Viewport(gengamedat.width / 2, 0, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[2] = new Viewport(0, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
                segment.viewports[3] = new Viewport(gengamedat.width / 2, gengamedat.height / 2, gengamedat.width / 2, gengamedat.height / 2);
            }
        }

        public void HandlePlayerCollisions(GeneralGameData dat, GameSegment segment)
        {
            //CollisionDetection.HandlePlayerCollision(dat,   segment);
            //CollisionDetection.Perform_Wall_Repel_Point(dat,   segment);
        }

        #region MARKED FOR REMOVAL NOT USED CODE
        public void VehicleCollision(GeneralGameData gengamedat, GameSegment segment)
        {

            //------------------------------------------------------------------------------------//
        }
        #endregion

        public void CreatePerspectives(GeneralGameData gamedat, GameSegment segment)
        {
            for (int i = 0; i < gamedat.PlayerCount; i++)
            {
                segment.Perspectives.Add(Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, gamedat.AspectRatio, gamedat.ViewDisMin, gamedat.ViewDisMax));
                segment.Perspectives.Add(Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4 / 1.5f, gamedat.AspectRatio, gamedat.ViewDisMin, gamedat.ViewDisMax));
                segment.Perspectives.Add(Matrix.CreateOrthographicOffCenter(-1, 1, -1, 1, 0, 1000));
            }
        }

        public void CreatePlayers(GeneralGameData gamedata, GameSegment segment)
        {
            for (int i = 0; i < gamedata.PlayerCount; i++)
            {
                Player player = new Player();
                player.firearm_current = segment.generic_firearms[0];
                Camera cam = new Camera();
                player.PlayerNumber = i;
                player.Set_PlayerPosition(new Vector3(-20, 52, 0));
                player.Set_PlayerRotation(new Vector3(0, 0, 0));
                player.Set_UpVector(Vector3.Up);
                player.updateViewMatrix();

                player.WalkSpeed_Walking = 1.0f;
                player.WalkSpeed_Sneak = 0.3f;
                player.WalkSpeed_Running = 2.0f;

                player.Walkspeed_Current = player.WalkSpeed_Walking;

                cam.Set_Position(player.Get_PlayerPosition());
                cam.Set_Rotation(Vector3.Add(player.Get_PlayerRotation(), new Vector3(0, 0, 0)));
                cam.Set_Projection(segment.Perspectives[0]);
                cam.CameraUpVector_Static = Vector3.Up;
                cam.updateViewMatrix();
                cam.CameraRelativePosition_Set = new Vector3(0, gamedata.PlayerHeight, 0);

                player.SetCamera(cam);
                segment.Players.Add(player);

                segment.Players[i].ViewGun_AnimController = new AnimationController(segment.shared_animation_guns, segment.gunLoadInfos[0].idleAnimationName, "Player " + i);
                segment.Players[i].WorldModel_AnimController = new AnimationController(segment.shared_animation_human, segment.playerLoadInfos[0].idleAnimationName_pistol, "Player " + i);

                segment.Players[i].CreateColliders(new SphereConvexHull(segment.Players[i].Position_Player, 2));
                segment.Players[i].firearm_current = segment.generic_firearms[0];
                segment.Players[i].playerModelNumber = 0;
            }
        }

        public void LoadMap(GeneralGameData gamedat, GameSegment segment)
        {
            Console.WriteLine("Loading map blocks");
            LoadMapBlocks(gameData.GameDirectory, gameData.RelativeMapFileAndPath, segment);

            Console.WriteLine("Loading NPC nodes ");
            Load_NPC_NodesAndMesh(segment, gameData);

            //Console.WriteLine("Loading load MapMeshes");
            //LoadMapMeshes(gameData.GameDirectory, gameData.RelativeMapFileAndPath,   segment);

            Console.WriteLine("Loading Skybox");
            LoadSkyBox(gameData.GameDirectory, "sky_cube", segment);
                                  

            InitialiseVertexBuffersForMapMeshes(gamedat, segment);
        }

        public void InitialiseVertexBuffersForMapMeshes(GeneralGameData gamedat, GameSegment segment)
        {
            for (int i = 0; i < segment.Level.MapMeshes.Count; i++)
            {
                segment.Level.MapMeshes[i].CreateBuffers(GraphicsDevice, segment.Level.MapMeshes[i].vertices.Length);
            }
        }

        public void DrawSetupUpfront(GameSegment segment, GeneralGameData gamedata)
        {
            //-----------------------------------------------------------------------//


            //IMPORTANT GRAPHIC SETTINGS TO RENDER 3D AND 2D
            //USING THE SAME VERTEXBUFFER, Spritebatch will
            //RESET THE SETTINGS EVERY TIME ITS CALLED


            RasterizerState rasterizerState1 = new RasterizerState();
            DepthStencilState depthStencilState = new DepthStencilState();
            rasterizerState1.CullMode = CullMode.CullClockwiseFace;

            depthStencilState = DepthStencilState.Default;
            BlendState b = new BlendState();

            b = BlendState.Opaque;
            GraphicsDevice.BlendState = b;


            this.GraphicsDevice.DepthStencilState = depthStencilState;
            this.GraphicsDevice.RasterizerState = rasterizerState1;

            GraphicsDeviceManager.ApplyChanges();

            //-----------------------------------------------------------------------//
        }

        public void CreateSpriteBatch(GameSegment seg, GeneralGameData dat)
        {
            seg.spritebatch = new SpriteBatch(this.GraphicsDevice);
            seg.fonts.Add(Content.Load<SpriteFont>("Content//Hudfont"));
            seg.fonts.Add(Content.Load<SpriteFont>("Content//Hudfont_half"));
            seg.fonts.Add(Content.Load<SpriteFont>("Content//Hudfont_1over4"));
        }

        public void UpdateHudData(GameSegment seg, GeneralGameData dat)
        {
            for (int i = 0; i < dat.PlayerCount; i++)
            {
                seg.hudsettings.hudposition[i].ArmorHud = Convert.ToString(seg.Players[i].Armor);
                seg.hudsettings.hudposition[i].HealthHud = Convert.ToString(seg.Players[i].Health);
                seg.hudsettings.hudposition[i].BulletsHud = Convert.ToString(1);
                //GET AMMO OF CURRENT HERE
                seg.hudsettings.hudposition[i].MagsHud = Convert.ToString(1);
            }
        }

        public void CreateHudData(GameSegment seg, GeneralGameData dat)
        {
            #region p1
            if (dat.PlayerCount == 1)
            {

                Vector2 hpos = new Vector2(seg.viewports[0].Width - 60, seg.viewports[0].Height - 30);
                Vector2 apos = new Vector2(seg.viewports[0].Width - 150, seg.viewports[0].Height - 30);
                Vector2 mags = new Vector2(seg.viewports[0].X + 30, seg.viewports[0].Height - 30);
                Vector2 rounds = new Vector2(seg.viewports[0].X + 120, seg.viewports[0].Height - 30);

                HUD_Data huddata = new HUD_Data(hpos, apos, mags, rounds);
                huddata.ArmorHud = String.Empty;
                huddata.HealthHud = String.Empty;
                huddata.MagsHud = String.Empty;
                huddata.BulletsHud = String.Empty;
                seg.hudsettings.hudposition.Add(huddata);
            }
            #endregion

            #region p2_setup
            if (dat.PlayerCount == 2)
            {
                #region p1
                Vector2 hpos1 = new Vector2(seg.viewports[0].Width - 60, seg.viewports[0].Height - 15);
                Vector2 apos1 = new Vector2(seg.viewports[0].Width - 150, seg.viewports[0].Height - 15);
                Vector2 mags1 = new Vector2(seg.viewports[0].X + 30, seg.viewports[0].Height - 15);
                Vector2 rounds1 = new Vector2(seg.viewports[0].X + 120, seg.viewports[0].Height - 15);

                HUD_Data huddata1 = new HUD_Data(hpos1, apos1, mags1, rounds1);

                huddata1.ArmorHud = String.Empty;
                huddata1.HealthHud = String.Empty;
                huddata1.MagsHud = String.Empty;
                huddata1.BulletsHud = String.Empty;
                #endregion

                #region p2
                Vector2 hpos2 = new Vector2(seg.viewports[1].Width - 60, seg.viewports[1].Height - 15);
                Vector2 apos2 = new Vector2(seg.viewports[1].Width - 150, seg.viewports[1].Height - 15);
                Vector2 mags2 = new Vector2(seg.viewports[1].X + 30, seg.viewports[1].Height - 15);
                Vector2 rounds2 = new Vector2(seg.viewports[1].X + 120, seg.viewports[1].Height - 15);


                HUD_Data huddata2 = new HUD_Data(hpos2, apos2, mags2, rounds2);

                huddata2.ArmorHud = String.Empty;
                huddata2.HealthHud = String.Empty;
                huddata2.MagsHud = String.Empty;
                huddata2.BulletsHud = String.Empty;
                #endregion

                seg.hudsettings.hudposition.Add(huddata1);
                seg.hudsettings.hudposition.Add(huddata2);
            }
            #endregion

            #region p3-4_setup
            if (dat.PlayerCount > 2)
            {
                #region p1
                Vector2 hpos1 = new Vector2(seg.viewports[0].Width - 50, seg.viewports[0].Height - 20);
                Vector2 apos1 = new Vector2(seg.viewports[0].Width - 115, seg.viewports[0].Height - 20);
                Vector2 mags1 = new Vector2(seg.viewports[0].X + 15, seg.viewports[0].Height - 20);
                Vector2 rounds1 = new Vector2(seg.viewports[0].X + 80, seg.viewports[0].Height - 20);

                HUD_Data huddata1 = new HUD_Data(hpos1, apos1, mags1, rounds1);
                HUD_Data huddata2 = new HUD_Data(hpos1, apos1, mags1, rounds1);
                HUD_Data huddata3 = new HUD_Data(hpos1, apos1, mags1, rounds1);
                HUD_Data huddata4 = new HUD_Data(hpos1, apos1, mags1, rounds1);

                huddata1.ArmorHud = String.Empty;
                huddata1.HealthHud = String.Empty;
                huddata1.MagsHud = String.Empty;
                huddata1.BulletsHud = String.Empty;

                huddata2.ArmorHud = String.Empty;
                huddata2.HealthHud = String.Empty;
                huddata2.MagsHud = String.Empty;
                huddata2.BulletsHud = String.Empty;

                huddata3.ArmorHud = String.Empty;
                huddata3.HealthHud = String.Empty;
                huddata3.MagsHud = String.Empty;
                huddata3.BulletsHud = String.Empty;

                huddata4.ArmorHud = String.Empty;
                huddata4.HealthHud = String.Empty;
                huddata4.MagsHud = String.Empty;
                huddata4.BulletsHud = String.Empty;
                #endregion

                seg.hudsettings.hudposition.Add(huddata1);
                seg.hudsettings.hudposition.Add(huddata2);
                seg.hudsettings.hudposition.Add(huddata3);
                seg.hudsettings.hudposition.Add(huddata4);
            }
            #endregion

        }

        float testbias = 0;

        //Draw Call
        public void DrawMeshgroup(GameSegment segment, GeneralGameData gamedat, int ShaderChocie)
        {
            GraphicsDevice.Clear(Color.Blue);
            bool drawSkybox = false,
                drawMapMeshes = true,
                drawMapMeshes_vb = true,
                drawPhysicsModels = false,
                drawMapBlocks = false,
                drawVehicles = false,
                renderMirror = false,
                renderShadows = true,
                drawAnimation = false,
                drawLightsOnly = false,
                drawSprites = false,
                drawGunSprites = false,
                drawParticles = false,
                Draw_Weapon_Models_World = true,
                Draw_Weapon_Models_View = true,
                Draw_NPC_Mesh = false,
                DrawNpcPath = true,
                DrawItems = false,
                DrawHudText = false,
                Draw_Players = true,
                Draw_Players_Hitbox = false,
                Draw_Pathfinding_Lines_01 = false,
                Draw_Pathfinding_Nodes_01 = true,
                Draw_Npc_Node_meshes = true;

            GraphicsDevice.SetRenderTarget(null);
            for (int i = 0; i < gamedat.PlayerCount; i++)
            {
                //Do this or it fails
                DrawSetupUpfront(segment, gamedat);

                BlendState b;

                GraphicsDevice.Viewport = segment.viewports[i];

                #region Render Shadow to Rendertargets
                if (renderShadows && false)
                {

                    for (int lc = 0; lc < segment.Level.mapLights.Length; lc++)
                    {
                        GraphicsDevice.SetRenderTarget(segment.Level.mapLights[lc].lightmap);
                        GraphicsDevice.Viewport = new Viewport(0, 0, 256, 256);
                        RasterizerState s = new RasterizerState();
                        s.CullMode = CullMode.None;
                        GraphicsDevice.RasterizerState = s;


                        for (int dd = 0; dd < segment.Level.visualMap.Length; dd++)
                        {

                            for (int f = 0; f < segment.Level.visualMap[dd].faces.Length; f++)
                            {
                                segment.Effects[6].CurrentTechnique = segment.Effects[6].Techniques["CreateShadowMap"];

                                segment.Effects[6].Parameters["World"].SetValue(Matrix.Identity);
                                segment.Effects[6].Parameters["View"].SetValue(segment.Level.mapLights[lc].viewDirected);
                                segment.Effects[6].Parameters["Projection"].SetValue(segment.Level.mapLights[lc].projection);

                                foreach (EffectPass eff in segment.Effects[6].CurrentTechnique.Passes)
                                {
                                    eff.Apply();

                                    GraphicsDevice.SetVertexBuffer(segment.Level.visualMap[dd].faces[f].vertexbuffer);

                                    GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.visualMap[dd].faces[f].vertexbuffer.VertexCount / 3);
                                }
                            }
                        }
                    }
                    GraphicsDevice.SetRenderTarget(null);
                    //Shadow map renders fine
                }

                #endregion

                #region Render Mirror to Rendertargets
                if (renderMirror)
                {
                    //--------------------------------------------------------------
                    GraphicsDevice.SetRenderTarget(segment.Mirrors[0].RenderTarget);

                    for (int j = 0; j < segment.Level.MapSkyboxes.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(Segment01.Level.MapSkyboxes[j].Mesh.TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.CreateWorld(segment.Players[j].Get_PlayerPosition(), Vector3.Forward, Vector3.Up));
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].PlayerCamera.Get_Positon());

                        foreach (EffectPass eff in segment.Effects[0].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapSkyboxes[j].Mesh.vertices,
                                0,
                                segment.Level.MapSkyboxes[j].Mesh.vertices.Length / 3);
                        }
                    }

                    for (int j = 0; j < segment.Level.MapMeshes.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTextureLighting"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Mirrors[0].Get_Mirror_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Mirrors[0].Position);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);

                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                            VectorMath.Vector3_Rotate(
                            segment.Players[i].GetCamera().Get_LookAt(),
                            segment.Players[i].GetCamera().Get_Rotation()));

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapMeshes[j].vertices,
                                0,
                                segment.Level.MapMeshes[j].vertices.Length / 3);
                        }
                    }
                }

                #endregion
                
                #region DrawMeshGroupsWithShadowMap
                if (false)
                {
                    //--------------------------------------
                    for (int j = 0; j < segment.Level.MapMeshes.Count; j++)
                    {
                        segment.Effects[3].CurrentTechnique = segment.Effects[3].Techniques["Blinn"];
                        segment.Effects[3].Parameters["SpecularRoughness"].SetValue(45);
                        segment.Effects[3].Parameters["SpecularIntensity"].SetValue(1f);

                        segment.Effects[3].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[3].Parameters["NormalTexture"].SetValue(segment.Level.MapMeshes[j].TextureBump);
                        segment.Effects[3].Parameters["SpecularTexture"].SetValue(segment.Level.MapMeshes[j].TextureSpecular);

                        Matrix mat1 = Matrix.CreateWorld(Vector3.Zero, new Vector3(1, 0, 0), new Vector3(0, MathHelper.PiOver2, 0));
                        Matrix mat2 = Matrix.CreateWorld(Vector3.Zero, new Vector3(0, 1, 0), new Vector3(0, 0, MathHelper.PiOver2));
                        Matrix mat3 = Matrix.CreateWorld(Vector3.Zero, new Vector3(0, 0, 1), new Vector3(MathHelper.PiOver2, 0, 0));
                        segment.Effects[3].Parameters["normalmatrix1"].SetValue(mat1);
                        segment.Effects[3].Parameters["normalmatrix2"].SetValue(mat2);
                        segment.Effects[3].Parameters["normalmatrix2"].SetValue(mat3);

                        segment.Effects[3].Parameters["LightMVP"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix() *
                            segment.Players[i].GetCamera().Get_Projection());
                        
                        segment.Effects[3].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[3].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[3].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                        segment.Effects[3].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        segment.Effects[3].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[3].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[3].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);

                        segment.Effects[3].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        foreach (EffectPass eff in segment.Effects[3].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapMeshes[j].vertices,
                                0,
                                segment.Level.MapMeshes[j].vertices.Length / 3);
                        }
                    }
                }

                #endregion

                #region Some shadow stuff
                if (false)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.RasterizerState = s;
                    //--------------------------------------
                    for (int j = 0; j < segment.Level.MapMeshes.Count; j++)
                    {
                        segment.Effects[4].CurrentTechnique = segment.Effects[4].Techniques["DrawWithShadowMap"];
                        Microsoft.Xna.Framework.Input.KeyboardState k = Microsoft.Xna.Framework.Input.Keyboard.GetState();
                        if (k.IsKeyDown(Keys.OemPlus))
                        {
                            testbias += 0.00001f;
                        }

                        if (k.IsKeyDown(Keys.OemMinus))
                        {
                            testbias -= 0.00001f;
                        }
                        segment.Effects[4].Parameters["DepthBias"].SetValue(testbias);
                        segment.Effects[4].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[4].Parameters["ShadowMap"].SetValue(segment.Shadows[0].RenderTarget);
                        segment.Effects[4].Parameters["LightViewProj"].SetValue(segment.Shadows[0].ViewMatrix *
                            segment.Shadows[0].ProjectionShadow);

                        segment.Effects[4].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[4].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[4].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                        segment.Effects[4].Parameters["LightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[4].Parameters["ShadowOn"].SetValue(1);

                        segment.Effects[4].Parameters["SpecularRoughness"].SetValue(80);
                        segment.Effects[4].Parameters["SpecularIntensity"].SetValue(2f);

                        segment.Effects[4].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[4].Parameters["NormalTexture"].SetValue(segment.Level.MapMeshes[j].TextureBump);
                        segment.Effects[4].Parameters["SpecularTexture"].SetValue(segment.Level.MapMeshes[j].TextureSpecular);

                        Matrix mat1 = Matrix.CreateWorld(Vector3.Zero, new Vector3(1, 0, 0), new Vector3(0, MathHelper.PiOver2, 0));
                        Matrix mat2 = Matrix.CreateWorld(Vector3.Zero, new Vector3(0, 1, 0), new Vector3(0, 0, MathHelper.PiOver2));
                        Matrix mat3 = Matrix.CreateWorld(Vector3.Zero, new Vector3(0, 0, 1), new Vector3(MathHelper.PiOver2, 0, 0));
                        segment.Effects[4].Parameters["normalmatrix1"].SetValue(mat1);
                        segment.Effects[4].Parameters["normalmatrix2"].SetValue(mat2);
                        segment.Effects[4].Parameters["normalmatrix2"].SetValue(mat3);

                        segment.Effects[4].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[4].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[4].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                        segment.Effects[4].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        foreach (EffectPass eff in segment.Effects[4].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapMeshes[j].vertices,
                                0,
                                segment.Level.MapMeshes[j].vertices.Length / 3);
                        }
                    }
                }
                #endregion

                #region draw Meshgroups
                if (segment.Level.draw_normal && false)
                {
                    segment.Effects[5].CurrentTechnique = segment.Effects[5].Techniques["PHONG_WITH_CUBE"];
                    //---------------------------------------------------------------------------------------
                    for (int j = 0; j < segment.Level.MapMeshes.Count; j++)
                    {
                        if (segment.Level.MapMeshes[j].transparencyFlag)
                            continue;
                                               

                        segment.Effects[5].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[5].Parameters["NormalTexture"].SetValue(segment.Level.MapMeshes[j].TextureBump);
                        segment.Effects[5].Parameters["SpecularTexture"].SetValue(segment.Level.MapMeshes[j].TextureSpecular);

                        segment.Effects[5].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[5].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[5].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                        segment.Effects[5].Parameters["normalPower"].SetValue(1);

                        segment.Effects[5].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[5].Parameters["DiffuseLightColorAndIntensity"].SetValue(gamedat.DiffuseLightColor * gamedat.DiffuseLightIntensity);
                        segment.Effects[5].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        segment.Effects[5].Parameters["World"].SetValue(Matrix.CreateRotationY(MathHelper.TwoPi));
                        foreach (EffectPass eff in segment.Effects[5].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapMeshes[j].vertices,
                                0,
                                segment.Level.MapMeshes[j].vertices.Length / 3);
                            //GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.MapMeshes.Count / 3);                        
                        }
                    }

                    //---------------------------------------------------------------------------------------
                    for (int j = 0; j < segment.Level.visualMap.Length; j++)
                    {
                        for (int f = 0; f < segment.Level.visualMap[j].faces.Length; f++)
                        {
                            segment.Effects[5].CurrentTechnique = segment.Effects[5].Techniques["PHONG_WITH_CUBE"];

                            segment.Effects[5].Parameters["ModelTexture"].SetValue(segment.Level.materials[segment.Level.visualMap[j].faces[f].shaderindex].color);

                            segment.Effects[5].Parameters["World"].SetValue(Matrix.CreateTranslation(segment.Level.visualMap[j].offset));
                            segment.Effects[5].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[5].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                            segment.Effects[5].Parameters["normalPower"].SetValue(1);

                            segment.Effects[5].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                            segment.Effects[5].Parameters["DiffuseLightColorAndIntensity"].SetValue(gamedat.DiffuseLightColor * gamedat.DiffuseLightIntensity);
                            segment.Effects[5].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity * 1);

                            foreach (EffectPass eff in segment.Effects[5].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.SetVertexBuffer(segment.Level.visualMap[j].faces[f].vertexbuffer);
                                
                                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.visualMap[j].faces[f].vertexbuffer.VertexCount / 3);
                            }
                        }
                    }
                }
                #endregion

                #region draw Meshgroups containing Alpha textures
                if (drawMapMeshes)
                {

                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.BlendState = BlendState.AlphaBlend;

                    GraphicsDevice.RasterizerState = s;


                    //---------------------------------------------------------------------------------------
                    for (int j = 0; j < segment.Level.MapMeshes.Count; j++)
                    {
                        if (!segment.Level.MapMeshes[j].transparencyFlag)
                            continue;

                        segment.Effects[5].CurrentTechnique = segment.Effects[5].Techniques["PHONG_WITH_CUBE"];

                        segment.Effects[5].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[5].Parameters["NormalTexture"].SetValue(segment.Level.MapMeshes[j].TextureBump);
                        segment.Effects[5].Parameters["SpecularTexture"].SetValue(segment.Level.MapMeshes[j].TextureSpecular);

                        segment.Effects[5].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[5].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[5].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                        segment.Effects[5].Parameters["normalPower"].SetValue(1);

                        segment.Effects[5].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[5].Parameters["DiffuseLightColorAndIntensity"].SetValue(gamedat.DiffuseLightColor * gamedat.DiffuseLightIntensity);
                        segment.Effects[5].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity * 0);

                        foreach (EffectPass eff in segment.Effects[5].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapMeshes[j].vertices,
                                0,
                                segment.Level.MapMeshes[j].vertices.Length / 3);
                            //GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.MapMeshes.Count / 3);                        
                        }
                    }

                    s = new RasterizerState();

                    GraphicsDevice.BlendState = BlendState.Opaque;

                    GraphicsDevice.RasterizerState = s;
                }
                #endregion

                #region draw meshgroups using vertexbuffer
                if (segment.Level.draw_vb && true)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.BlendState = BlendState.AlphaBlend;

                    GraphicsDevice.RasterizerState = s;

                    //---------------------------------------------------------------------------------------
                    for (int j = 0; j < segment.Level.visualMap.Length; j++)
                    {
                        for (int f = 0; f < segment.Level.visualMap[j].faces.Length; f++)
                        {
                            segment.Effects[5].CurrentTechnique = segment.Effects[5].Techniques["PHONG_WITH_CUBE"];

                            segment.Effects[5].Parameters["ModelTexture"].SetValue(segment.Level.materials[segment.Level.visualMap[j].faces[f].shaderindex].color);

                            segment.Effects[5].Parameters["World"].SetValue(Matrix.CreateTranslation(segment.Level.visualMap[j].offset));
                            segment.Effects[5].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[5].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                            segment.Effects[5].Parameters["normalPower"].SetValue(1);

                            segment.Effects[5].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                            segment.Effects[5].Parameters["DiffuseLightColorAndIntensity"].SetValue(gamedat.DiffuseLightColor * gamedat.DiffuseLightIntensity);
                            segment.Effects[5].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity * 1);

                            foreach (EffectPass eff in segment.Effects[5].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.SetVertexBuffer(segment.Level.visualMap[j].faces[f].vertexbuffer);
                                
                                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.visualMap[j].faces[f].vertexbuffer.VertexCount / 3);
                            }
                        }
                    }

                    s = new RasterizerState();

                    GraphicsDevice.BlendState = BlendState.Opaque;

                    GraphicsDevice.RasterizerState = s;
                }
                #endregion

                #region draw meshgroups using vertexbuffer with shadows
                if (segment.Level.draw_vb && false)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.BlendState = BlendState.AlphaBlend;

                    GraphicsDevice.RasterizerState = s;
                    segment.Effects[7].CurrentTechnique = segment.Effects[7].Techniques["DrawWithShadowMap"];
                    //---------------------------------------------------------------------------------------
                    for (int j = 0; j < segment.Level.visualMap.Length; j++)
                    {
                        for (int f = 0; f < segment.Level.visualMap[j].faces.Length; f++)
                        {

                            segment.Effects[7].Parameters["LightViewProj"].SetValue(segment.Players[i].PlayerCamera.Get_ViewMatrix() *
                                segment.Players[i].PlayerCamera.Get_Projection());

                            segment.Effects[7].Parameters["ShadowMap"].SetValue(segment.Level.mapLights[0].lightmap);
                            
                            segment.Effects[7].Parameters["ModelTexture"].SetValue(segment.Level.materials[segment.Level.visualMap[j].faces[f].shaderindex].color);
                            segment.Effects[7].Parameters["World"].SetValue(Matrix.CreateTranslation(segment.Level.visualMap[j].offset));
                            segment.Effects[7].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[7].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                           
                            segment.Effects[7].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                            segment.Effects[7].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                            segment.Effects[7].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                            segment.Effects[7].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);

                            segment.Effects[7].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity * 1);

                            foreach (EffectPass eff in segment.Effects[7].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.SetVertexBuffer(segment.Level.visualMap[j].faces[f].vertexbuffer);

                                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, segment.Level.visualMap[j].faces[f].vertexbuffer.VertexCount / 3);
                            }
                        }
                    }

                    s = new RasterizerState();

                    GraphicsDevice.BlendState = BlendState.Opaque;

                    GraphicsDevice.RasterizerState = s;
                }
                #endregion


                #region Draw particles
                if (drawParticles)
                {
                    /*
                    //--------------------------------------
                    for (int j = 0; j < segment.ParticlePoolCollection.Count; j++)
                    {
                        segment.ParticlePoolCollection[0].UpdateParticlePool(segment.Players[i].GetCamera().Get_Positon());
                        for (int d = 0; d < segment.ParticlePoolCollection[0].Particles.Length; d++)
                        {
                            Matrix m = Matrix.CreateWorld(segment.ParticlePoolCollection[0].Particles[d].Position, (segment.Players[i].GetCamera().Get_Positon() - segment.ParticlePoolCollection[0].Particles[d].Position), Vector3.Up);

                            segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTexture"];
                            segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.ParticlePoolCollection[0].ParticleTexture);
                            segment.Effects[ShaderChocie].Parameters["World"].SetValue(m);
                            segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                            foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                            {
                                eff.Apply();
                                GraphicsDevice.DrawUserPrimitives<EffectVertex>(
                                    PrimitiveType.TriangleList, segment.ParticlePoolCollection[0].Particles[d].ParticlePlane,
                                    0,
                                    segment.ParticlePoolCollection[0].Particles[d].ParticlePlane.Length / 3);

                            }
                        }
                    }
                    b = new BlendState();
                    b.AlphaSourceBlend = Blend.Zero;
                    GraphicsDevice.BlendState = b;
                    GraphicsDeviceManager.ApplyChanges();
                    */
                }
                #endregion

                #region DrawVehicles
                if (drawVehicles)
                {
                    //--------------------------------------
                    for (int j = 0; j < segment.Vehicles.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["Phong"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Vehicles[j].physicsModel.visualMesh.TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["NormalTexture"].SetValue(segment.Vehicles[j].physicsModel.visualMesh.TextureBump);
                        segment.Effects[ShaderChocie].Parameters["SpecularTexture"].SetValue(segment.Vehicles[j].physicsModel.visualMesh.TextureSpecular);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(segment.Vehicles[j].physicsModel.MeshMatrix);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                            VectorMath.Vector3_Rotate(
                            segment.Players[i].GetCamera().Get_LookAt(),
                            segment.Players[i].GetCamera().Get_Rotation()));

                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                        segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Vehicles[j].physicsModel.visualMesh.vertices,
                                0,
                                segment.Vehicles[j].physicsModel.visualMesh.vertices.Length / 3);
                        }
                    }
                }
                #endregion

                #region Draw Weapon Models World
                if (Draw_Weapon_Models_World)
                {
                    if (segment.playerModels[segment.Players[i].firearm_current.worldModelIndex] != null)
                    {
                        for (int j = 0; j < segment.Players.Count; j++)
                        {
                            if (i == j)
                                continue;


                            if (segment.Players[j].WorldModel_AnimController.PlayAnimationOnce)
                            {
                                segment.Players[j].WorldModel_AnimController.AnimationTimer++;

                                Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_human,
                                    segment.Players[j].WorldModel_AnimController.currentAnimationName,
                                   segment.weaponModels_World[0],
                                   segment.Players[j].WorldModel_AnimController,
                                   Matrix.CreateRotationY(-segment.Players[j].Get_PlayerRotation().Y + MathHelper.TwoPi) * Matrix.CreateTranslation(segment.Players[j].Get_PlayerPosition() + new Vector3(0, 4f, 0)),
                                    "spine1", Matrix.CreateRotationX(-segment.Players[j].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(-segment.Players[j].LeftRightBend_adapt.Z / 1.5f),
                                    "arm_right", Matrix.CreateRotationX(segment.Players[j].GetCamera().Get_Rotation().X / 1.5f),
                                    "arm_left", Matrix.CreateRotationX(segment.Players[j].GetCamera().Get_Rotation().X / 1.5f),
                                    null);
                            }
                            //This plays the idle Animation if noone is played else
                            else if (segment.Players[j].WorldModel_AnimController.PlayAnimationOnce == false)
                            {
                                Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_human,
                                   segment.Players[j].WorldModel_AnimController.idleAnimName,
                                   segment.weaponModels_World[0],
                                   segment.Players[j].WorldModel_AnimController,
                                   Matrix.CreateRotationY(-segment.Players[j].Get_PlayerRotation().Y + MathHelper.Pi) * Matrix.CreateTranslation(segment.Players[j].Get_PlayerPosition() + new Vector3(0, 0.4f, 0)),
                                   "spine1", Matrix.CreateRotationX(-segment.Players[j].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(-segment.Players[j].LeftRightBend_adapt.Z / 1.5f),
                                    "arm_right", Matrix.CreateRotationX(segment.Players[j].GetCamera().Get_Rotation().X / 1.5f),
                                    "arm_left", Matrix.CreateRotationX(segment.Players[j].GetCamera().Get_Rotation().X / 1.5f),
                                    null);

                                segment.Players[j].WorldModel_AnimController.AnimationTimer++;
                            }


                            RasterizerState rast = new RasterizerState();
                            rast.CullMode = CullMode.CullClockwiseFace;
                            GraphicsDevice.RasterizerState = rast;

                            //-------------------------------------------------------------------------------     
                            segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];
                            segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.weaponModels_World[segment.Players[i].firearm_current.worldModelIndex].DiffuseTexture);

                            segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                            segment.Effects[ShaderChocie].Parameters["flashLight"].SetValue(segment.FlashLightTexture);

                            segment.Effects[ShaderChocie].Parameters["LightView"].SetValue(segment.Players[i].PlayerCamera.Get_ViewMatrix());
                            segment.Effects[ShaderChocie].Parameters["LightProjection"].SetValue(Matrix.CreateOrthographic(2, 2, 0, 1000));

                            segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                                VectorMath.Vector3_Rotate(
                                segment.Players[i].GetCamera().Get_LookAt(),
                            segment.Players[i].GetCamera().Get_Rotation()));

                            segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                            segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                            segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                            segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                            segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                            segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                            segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                            foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList,
                                    segment.weaponModels_World[segment.Players[i].firearm_current.worldModelIndex].Vertices,
                                    0,
                                    segment.weaponModels_World[segment.Players[i].firearm_current.worldModelIndex].Vertices.Length / 3);
                            }
                        }
                    }
                }
                #endregion

                #region Draw Weapon Models View
                if (Draw_Weapon_Models_View)
                {
                    Matrix rotMat = Matrix.CreateRotationY(-segment.Players[i].Get_PlayerRotation().Y + MathHelper.Pi) * Matrix.CreateRotationZ(segment.Players[i].PlayerCamera.Get_Rotation().X);
                    Matrix roty = Matrix.CreateRotationY(-segment.Players[i].Get_PlayerRotation().Y + MathHelper.Pi);

                    Matrix transMat = Matrix.CreateTranslation(new Vector3(segment.Players[i].PlayerCamera.CameraUpVector_Final.X, 0, segment.Players[i].PlayerCamera.CameraUpVector_Final.Z) / 6 + segment.Players[i].PlayerCamera.Get_Positon() - new Vector3(0, 0.0f, 0));
                    Matrix rotZ = Matrix.CreateRotationX(segment.Players[i].PlayerCamera.Get_Rotation().X);

                    Matrix resMat = rotZ * roty * transMat;
                    
                    //Play a specific animation over the idle first
                    if (segment.Players[i].ViewGun_AnimController.PlayAnimationOnce)
                    {
                        segment.Players[i].ViewGun_AnimController.AnimationTimer++;

                        Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_guns,
                            segment.Players[i].ViewGun_AnimController.currentAnimationName,
                            segment.weaponModels_View[segment.Players[i].firearm_current.viewModelIndex],
                            segment.Players[i].ViewGun_AnimController,
                            resMat,
                            "", Matrix.Identity,
                            "", Matrix.Identity,
                            "", Matrix.Identity,
                            null);

                    }
                    //This plays the idle Animation if noone is played else
                    else if (segment.Players[i].ViewGun_AnimController.PlayAnimationOnce == false)
                    {
                        Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_guns,
                           segment.Players[i].ViewGun_AnimController.idleAnimName,
                           segment.weaponModels_View[segment.Players[i].firearm_current.viewModelIndex],
                           segment.Players[i].ViewGun_AnimController,
                           resMat,
                           "", Matrix.Identity,
                            "", Matrix.Identity,
                            "", Matrix.Identity,
                            null);

                        segment.Players[i].ViewGun_AnimController.AnimationTimer++;
                    }

                    RasterizerState rast = new RasterizerState();
                    rast.CullMode = CullMode.CullClockwiseFace;
                    GraphicsDevice.RasterizerState = rast;

                    //-------------------------------------------------------------------------------     
                    segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];
                    segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.weaponModels_World[segment.Players[i].firearm_current.viewModelIndex].DiffuseTexture);

                    segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                    segment.Effects[ShaderChocie].Parameters["flashLight"].SetValue(segment.FlashLightTexture);

                    segment.Effects[ShaderChocie].Parameters["LightView"].SetValue(segment.Players[i].PlayerCamera.Get_ViewMatrix());
                    segment.Effects[ShaderChocie].Parameters["LightProjection"].SetValue(Matrix.CreateOrthographic(2, 2, 0, 1000));

                    segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                        VectorMath.Vector3_Rotate(
                        segment.Players[i].GetCamera().Get_LookAt(),
                        segment.Players[i].GetCamera().Get_Rotation()));

                    segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                    segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                    segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                    segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                    segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                    segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                    segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                    segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                    foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                    {
                        eff.Apply();

                        GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                            PrimitiveType.TriangleList,
                            segment.weaponModels_View[segment.Players[i].firearm_current.viewModelIndex].Vertices,
                            0,
                            segment.weaponModels_View[segment.Players[i].firearm_current.viewModelIndex].Vertices.Length / 3);


                    }

                }
                #endregion

                #region Draw Animated models
                if (Draw_NPC_Mesh)
                {
                    if (segment.npc_level.Npcs.Count > 0)
                    {
                        RasterizerState rast = new RasterizerState();
                        rast.CullMode = CullMode.CullCounterClockwiseFace;
                        GraphicsDevice.RasterizerState = rast;

                        //-------------------------------------------------------------------------------     
                        segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.npc_level.Npcs[0].model.DiffuseTexture);
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        segment.Effects[ShaderChocie].Parameters["flashLight"].SetValue(segment.FlashLightTexture);

                        segment.Effects[ShaderChocie].Parameters["LightView"].SetValue(segment.Players[i].PlayerCamera.Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["LightProjection"].SetValue(Matrix.CreateOrthographic(2, 2, 0, 1000));

                        segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                            VectorMath.Vector3_Rotate(
                            segment.Players[i].GetCamera().Get_LookAt(),
                            segment.Players[i].GetCamera().Get_Rotation()));

                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                        segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList,
                                segment.npc_level.Npcs[0].model.Vertices,
                                0,
                                segment.npc_level.Npcs[0].model.Vertices.Length / 3);

                        }
                    }
                }
                #endregion

                #region Draw players
                if (Draw_Players)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.RasterizerState = s;

                    #region Animation playback handling Player
                    Matrix mat_hitbox = Matrix.CreateRotationY(-segment.Players[i].Get_PlayerRotation().Y + MathHelper.TwoPi) * Matrix.CreateTranslation(new Vector3(0, 0.4f, 0) + segment.Players[i].Position_Player);
                    Matrix mat_model = Matrix.CreateTranslation(0, 0.4f, 0);
                    //Play a specific animation over the idle first
                    if (segment.Players[i].WorldModel_AnimController.PlayAnimationOnce)
                    {
                        segment.Players[i].WorldModel_AnimController.AnimationTimer++;
                        //seg.Players[i].ViewGun_AnimController.AnimationTimer++;

                        Ret_MatrixFromAnimation_Type data;
                        data = Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_human,
                            segment.Players[i].WorldModel_AnimController.currentAnimationName,
                              segment.playerModels[segment.Players[i].playerModelNumber],
                              segment.Players[i].WorldModel_AnimController,
                              mat_model,
                            "spine1", Matrix.CreateRotationX(-segment.Players[i].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(-segment.Players[i].LeftRightBend_adapt.Z / 1.5f),
                            "arm_right", Matrix.CreateRotationX(segment.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                            "arm_left", Matrix.CreateRotationX(segment.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                            new List<string>() { "neck" })[0];
                    }
                    //This plays the idle Animation if noone is played else
                    else if (segment.Players[i].WorldModel_AnimController.PlayAnimationOnce == false)
                    {
                        segment.Players[i].WorldModel_AnimController.AnimationTimer++;

                        Animation_Functions.PlayAnimation_AnimColl(segment.shared_animation_human,
                            segment.Players[i].WorldModel_AnimController.idleAnimName,
                              segment.playerModels[segment.Players[i].playerModelNumber],
                              segment.Players[i].WorldModel_AnimController,
                              mat_model,
                            "spine1", Matrix.CreateRotationX(-segment.Players[i].GetCamera().Get_Rotation().X / 2) * Matrix.CreateRotationZ(-segment.Players[i].LeftRightBend_adapt.Z / 1.5f),
                            "arm_right", Matrix.CreateRotationX(segment.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                            "arm_left", Matrix.CreateRotationX(segment.Players[i].GetCamera().Get_Rotation().X / 1.5f),
                             new List<string>() { "spine1" });
                    }
                    else
                    {
                        segment.Players[i].WorldModel_AnimController.PlayAnimationOnce = false;
                    }

                    #endregion


                    for (int playerModel = 0; playerModel < gameData.PlayerCount; playerModel++)
                    {
                        if (playerModel != i)
                        {
                            //-------------------------------------------------------------------------------     
                            segment.Effects[5].CurrentTechnique = Segment01.Effects[5].Techniques["PHONG_WITH_CUBE"];
                            segment.Effects[5].Parameters["ModelTexture"].SetValue(segment.playerModels[0].DiffuseTexture);
                            segment.Effects[5].Parameters["NormalTexture"].SetValue(segment.playerModels[0].DiffuseTexture);
                            segment.Effects[5].Parameters["SpecularTexture"].SetValue(segment.playerModels[0].DiffuseTexture);
                         
                            segment.Effects[5].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                            segment.Effects[5].Parameters["DiffuseLightColorAndIntensity"].SetValue(gamedat.DiffuseLightColor * gamedat.DiffuseLightIntensity);
                            
                            segment.Effects[5].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                            segment.Effects[5].Parameters["World"].SetValue(Matrix.CreateRotationY(-segment.Players[playerModel].Get_PlayerRotation().Y + MathHelper.Pi) * Matrix.CreateTranslation(segment.Players[playerModel].Position_Player));
                            segment.Effects[5].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[5].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());


                            foreach (EffectPass eff in segment.Effects[5].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList,
                                    segment.playerModels[0].Vertices,
                                    0,
                                    segment.playerModels[0].Vertices.Length / 3);

                            }
                        }
                    }
                }
                #endregion

                #region Draw players hitbox
                if (Draw_Players_Hitbox)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.RasterizerState = s;

                    for (int playerModel = 0; playerModel < gameData.PlayerCount; playerModel++)
                    {
                        if (i == playerModel)
                            continue;
                        //-------------------------------------------------------------------------------     
                        segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.playerModels[0].DiffuseTexture);

                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        segment.Effects[ShaderChocie].Parameters["flashLight"].SetValue(segment.FlashLightTexture);

                        segment.Effects[ShaderChocie].Parameters["LightView"].SetValue(segment.Players[i].PlayerCamera.Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["LightProjection"].SetValue(Matrix.CreateOrthographic(2, 2, 0, 1000));

                        segment.Effects[ShaderChocie].Parameters["CameraLookAt"].SetValue(
                            VectorMath.Vector3_Rotate(
                            segment.Players[i].GetCamera().Get_LookAt(),
                            segment.Players[i].GetCamera().Get_Rotation()));


                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                        segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());


                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList,
                                segment.hitboxModels[0].Vertices,
                                0,
                                segment.hitboxModels[0].Vertices.Length / 3);

                        }
                    }
                }
                #endregion

                #region DrawMapBlocks
                if (drawMapBlocks)
                {
                    FillMode fill = FillMode.WireFrame;

                    RasterizerState ras = new RasterizerState();
                    ras.CullMode = CullMode.None;
                    ras.FillMode = fill;
                    GraphicsDevice.RasterizerState = ras;
                    GraphicsDeviceManager.ApplyChanges();
                    //--------------------------------------
                    for (int j = 0; j < segment.Level.MapBlocks.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["PhysicsBlocks"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(new Vector3(0.3f, 0.3f, 0.3f));

                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(new Vector4(0.6f, 0.6f, 1.0f, 1) * 0.2f);

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            if (segment.Level.MapBlocks[j].wallModel.vertices.Length > 0)
                            {
                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList, segment.Level.MapBlocks[j].wallModel.vertices,
                                    0,
                                    segment.Level.MapBlocks[j].wallModel.vertices.Length / 3);
                            }
                        }
                    }
                }
                #endregion

                #region Draw_Npc_Node_meshes
                if (Draw_Npc_Node_meshes)
                {
                    FillMode fill = FillMode.Solid;

                    RasterizerState ras = new RasterizerState();
                    ras.CullMode = CullMode.None;
                    ras.FillMode = fill;
                    GraphicsDevice.RasterizerState = ras;
                    GraphicsDeviceManager.ApplyChanges();
                    //--------------------------------------

                    for (int j = 0; j < segment.npc_level.Npc_Node_Collection_list[0].nodeBlock.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["PhysicsBlocks"];

                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(new Vector3(0.3f, 0.3f, 0.3f));

                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(new Vector4(0.6f, 0.6f, 1.0f, 1) * 0.2f);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);

                        if (segment.npc_level.Npc_Node_Collection_list[0].nodes[j].selected > 0)
                        {
                            //Console.WriteLine("Selected");
                        }
                        segment.Effects[ShaderChocie].Parameters["PhysblockSelected"].SetValue(segment.npc_level.Npc_Node_Collection_list[0].nodes[j].selected);
                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.npc_level.Npc_Node_Collection_list[0].nodeBlock[j].floorModel.vertices,
                                0,
                                segment.npc_level.Npc_Node_Collection_list[0].nodeBlock[j].floorModel.vertices.Length / 3);

                        }

                    }
                }
                #endregion

                #region Draw Npc walk path
                if (DrawNpcPath)
                {
                    FillMode fill = FillMode.WireFrame;

                    RasterizerState ras = new RasterizerState();
                    ras.CullMode = CullMode.None;
                    ras.FillMode = fill;
                    GraphicsDevice.RasterizerState = ras;
                    GraphicsDeviceManager.ApplyChanges();
                    //--------------------------------------
                    for (int j = 0; j < segment.Level.Npc_Walk_floor.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["PhysicsBlocks"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Level.MapMeshes[j].TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(new Vector3(0.3f, 0.3f, 0.3f));

                        
                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(new Vector4(0.6f, 0.6f, 1.0f, 1) * 0.2f);

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            if (segment.Level.Npc_Walk_floor[j].floorModel.vertices.Length > 0)
                            {
                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList, segment.Level.Npc_Walk_floor[j].floorModel.vertices,
                                    0,
                                    segment.Level.Npc_Walk_floor[j].floorModel.vertices.Length / 3);
                            }
                        }
                    }
                }
                #endregion

                #region DrawPhysmodel

                if (drawPhysicsModels)
                {
                    //--------------------------------------
                    for (int j = 0; j < segment.PhysicsModels.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["Phong"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.PhysicsModels[j].visualMesh.TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.Identity);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        segment.Effects[ShaderChocie].Parameters["DiffuseLightDirection"].SetValue(gamedat.DiffuseLightDirection);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightColor"].SetValue(gamedat.DiffuseLightColor);
                        segment.Effects[ShaderChocie].Parameters["DiffuseLightIntensity"].SetValue(gamedat.DiffuseLightIntensity);
                        segment.Effects[ShaderChocie].Parameters["SpecularReflectivity"].SetValue(gamedat.SpecularReflectivity);
                        segment.Effects[ShaderChocie].Parameters["AmbientLightAndIntensity"].SetValue(gamedat.AmbientLightAndIntensity);

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.PhysicsModels[0].visualMesh.vertices,
                                0,
                                segment.PhysicsModels[0].visualMesh.vertices.Length / 3);
                        }
                    }
                }
                #endregion

                //WILL DEFO USE THIS AGAIN 
                #region Draw Npc Nodes
                /*
                if (DrawNpcNodes)
                {
                    //--------------------------------------
                    
                    segment.Npc_Path_collection.rotateY += 0.015f;

                    for (int j = 0; j < segment.Npc_Path_collection.path_tracks.Count; j++)
                    {
                        for (int k = 0; k < segment.Npc_Path_collection.path_tracks[j].path.Count; k++)
                        {
                            segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTexture"];
                            segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Npc_Path_collection.NodeMesh.TextureDiffuse);
                            segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.CreateRotationY(segment.Npc_Path_collection.rotateY) * Matrix.CreateTranslation(segment.Npc_Path_collection.path_tracks[j].path[k].Position));
                            segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                            segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());

                            foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                            {
                                eff.Apply();
                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList, segment.Npc_Path_collection.NodeMesh.vertices,
                                    0,
                                   segment.Npc_Path_collection.NodeMesh.vertices.Length / 3);

                            }
                        }
                    }
                    b = new BlendState();
                    b = BlendState.Opaque;
                    GraphicsDevice.BlendState = b;
                    GraphicsDeviceManager.ApplyChanges();
                }               
                */
                #endregion

                #region DrawSkyboxes
                if (drawSkybox)
                {
                    b = new BlendState();
                    b = BlendState.Opaque;
                    CullMode cull = CullMode.None;
                    RasterizerState ras = new RasterizerState();
                    ras.CullMode = cull;
                    GraphicsDevice.RasterizerState = ras;
                    GraphicsDevice.BlendState = b;
                    GraphicsDeviceManager.ApplyChanges();
                    //--------------------------------------
                    for (int j = 0; j < segment.Level.MapSkyboxes.Count; j++)
                    {
                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["Transparency"].SetValue(1);
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.Level.MapSkyboxes[0].Mesh.TextureDiffuse);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(Matrix.CreateWorld(segment.Players[j].Get_PlayerPosition(), Vector3.Forward, Vector3.Up));
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].PlayerCamera.Get_Positon());

                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                PrimitiveType.TriangleList, segment.Level.MapSkyboxes[j].Mesh.vertices,
                                0,
                                segment.Level.MapSkyboxes[j].Mesh.vertices.Length / 3);
                        }
                    }
                }
                #endregion

                #region Drawitems
                if (DrawItems)
                {
                    BlendState be;
                    RasterizerState ras;
                    FillMode fill = FillMode.Solid;

                    //--------------------------------------
                    for (int j = 0; j < segment.itemsInMap.Count; j++)
                    {
                        if ((segment.itemsInMap[j] is HealthPack) && segment.itemsInMap[j].ItemSpawned())
                        {
                            be = BlendState.Opaque;
                            GraphicsDevice.BlendState = be;
                            ras = new RasterizerState();
                            ras.CullMode = CullMode.CullClockwiseFace;
                            ras.FillMode = fill;
                            GraphicsDevice.RasterizerState = ras;

                            GraphicsDeviceManager.ApplyChanges();
                        }
                        else if ((segment.itemsInMap[j] is BodyArmor) && segment.itemsInMap[j].ItemSpawned())
                        {
                            be = BlendState.Opaque;
                            GraphicsDevice.BlendState = be;
                            ras = new RasterizerState();
                            ras.CullMode = CullMode.CullClockwiseFace;
                            ras.FillMode = fill;
                            GraphicsDevice.RasterizerState = ras;

                            GraphicsDeviceManager.ApplyChanges();
                        }
                        else if (segment.itemsInMap[j].ItemSpawned())
                        {
                            be = BlendState.Additive;
                            GraphicsDevice.BlendState = be;
                            ras = new RasterizerState();
                            ras.CullMode = CullMode.CullCounterClockwiseFace;
                            ras.FillMode = fill;
                            GraphicsDevice.RasterizerState = ras;

                            GraphicsDeviceManager.ApplyChanges();
                        }


                        segment.Effects[ShaderChocie].CurrentTechnique = Segment01.Effects[ShaderChocie].Techniques["SimpleTexture"];

                        segment.Effects[ShaderChocie].Parameters["Transparency"].SetValue(1f);
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.itemsInMap[j].Get_Item_Mesh().DiffuseTexture);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(segment.itemsInMap[j].Get_ItemMatrix());
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        segment.Effects[ShaderChocie].Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());

                        if (segment.itemsInMap[j].ItemSpawned())
                        {
                            foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                            {
                                eff.Apply();

                                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                                    PrimitiveType.TriangleList, segment.itemsInMap[j].Get_Item_Mesh().Vertices,
                                    0,
                                    segment.itemsInMap[j].Get_Item_Mesh().Vertices.Length / 3);
                            }
                        }
                    }
                }
                #endregion

                Line l = new Line(segment.npc_level.playerpos, segment.npc_level.targetpos, segment.npc_level.activateColor);
                #region DrawLines 1
                if (Draw_Pathfinding_Nodes_01)
                {

                    l.LineVertices[0].Position = segment.npc_level.playerpos + new Vector3(0.1f, 0, 0);
                    l.LineVertices[1].Position = segment.npc_level.playerpos + new Vector3(-0.1f, 0, 0);
                    l.LineVertices[2].Position = segment.npc_level.targetpos;
                    
                    if (segment.npc_level.pathLines.Count <= 0)
                        continue;

                    for (int x = 0; x < segment.npc_level.pathLines[0].Lines.Count; x++)
                    {
                        RasterizerState ras = new RasterizerState();

                        ras.CullMode = CullMode.None;
                        GraphicsDevice.RasterizerState = ras;
                        //--------------------------------------

                        segment.npc_level.lineEffect.CurrentTechnique = segment.npc_level.lineEffect.Techniques[0];

                        segment.npc_level.lineEffect.Parameters["World"].SetValue(Matrix.Identity);
                        segment.npc_level.lineEffect.Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.npc_level.lineEffect.Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        //segment.npc_level.lineEffect.Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());
                        foreach (EffectPass eff in segment.npc_level.lineEffect.CurrentTechnique.Passes)
                        {

                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(
                            PrimitiveType.TriangleList, segment.npc_level.pathLines[0].Lines[x].LineVertices,
                            0,
                            1);


                            GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(
                          PrimitiveType.TriangleList, l.LineVertices,
                          0,
                          1);
                        }
                    }
                }
                #endregion

                #region DrawLines
                if (Draw_Pathfinding_Lines_01)
                {
                    if (segment.npc_level.pathLines.Count < 2)
                        continue;;

                    for (int x = 0; x < segment.npc_level.pathLines[1].Lines.Count; x++)
                    {
                        RasterizerState ras = new RasterizerState();

                        ras.CullMode = CullMode.None;
                        GraphicsDevice.RasterizerState = ras;
                        //--------------------------------------

                        segment.npc_level.lineEffect.CurrentTechnique = segment.npc_level.lineEffect.Techniques[0];

                        //segment.npc_level.lineEffect.Parameters["ModelTexture"].SetValue(segment.npc_level.line_texture);
                        segment.npc_level.lineEffect.Parameters["World"].SetValue(Matrix.Identity);
                        segment.npc_level.lineEffect.Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.npc_level.lineEffect.Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());
                        //segment.npc_level.lineEffect.Parameters["CameraPosition"].SetValue(segment.Players[i].GetCamera().Get_Positon());
                        foreach (EffectPass eff in segment.npc_level.lineEffect.CurrentTechnique.Passes)
                        {

                            eff.Apply();

                            GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(
                            PrimitiveType.TriangleList, segment.npc_level.pathLines[1].Lines[x].LineVertices,
                            0,
                            1);
                        }
                    }



                }
                #endregion

                #region drawhud
                if (DrawHudText)
                {
                    segment.spritebatch.Begin();
                    int k = i;
                    if (gameData.PlayerCount == 1)
                    {
                        segment.spritebatch.DrawString(segment.fonts[0], segment.hudsettings.hudposition[k].HealthHud, segment.hudsettings.hudposition[k].HealthPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[0], segment.hudsettings.hudposition[k].ArmorHud, segment.hudsettings.hudposition[k].ArmorPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[0], segment.hudsettings.hudposition[k].BulletsHud, segment.hudsettings.hudposition[k].BulletsLoadedPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[0], segment.hudsettings.hudposition[k].MagsHud, segment.hudsettings.hudposition[k].Magazines_RemainPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], "+", new Vector2(segment.viewports[0].Width / 2, segment.viewports[0].Height / 2), Color.White);
                    }

                    if (gameData.PlayerCount == 2)
                    {
                        segment.spritebatch.DrawString(segment.fonts[1], segment.hudsettings.hudposition[k].HealthHud, segment.hudsettings.hudposition[k].HealthPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[1], segment.hudsettings.hudposition[k].ArmorHud, segment.hudsettings.hudposition[k].ArmorPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[1], segment.hudsettings.hudposition[k].BulletsHud, segment.hudsettings.hudposition[k].BulletsLoadedPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[1], segment.hudsettings.hudposition[k].MagsHud, segment.hudsettings.hudposition[k].Magazines_RemainPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], "'", new Vector2(segment.viewports[0].Width / 2, segment.viewports[0].Height / 2), Color.White);
                    }

                    if (gameData.PlayerCount > 2)
                    {
                        segment.spritebatch.DrawString(segment.fonts[2], "/", new Vector2(segment.viewports[0].Width - 70, segment.viewports[0].Height - 20), Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], segment.hudsettings.hudposition[k].HealthHud, segment.hudsettings.hudposition[k].HealthPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], segment.hudsettings.hudposition[k].ArmorHud, segment.hudsettings.hudposition[k].ArmorPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], segment.hudsettings.hudposition[k].BulletsHud, segment.hudsettings.hudposition[k].BulletsLoadedPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], segment.hudsettings.hudposition[k].MagsHud, segment.hudsettings.hudposition[k].Magazines_RemainPos, Color.White);
                        segment.spritebatch.DrawString(segment.fonts[2], "+", new Vector2(segment.viewports[0].Width / 2, segment.viewports[0].Height / 2), Color.White);
                    }

                    segment.spritebatch.End();
                }
                #endregion

                #region DrawSprites
                if (drawSprites)
                {
                    RasterizerState s = new RasterizerState();
                    s.CullMode = CullMode.None;
                    GraphicsDevice.BlendState = BlendState.Additive;

                    GraphicsDevice.RasterizerState = s;

                    //--------------------------------------
                    for (int j = 0; j < segment.spriteContainer.Sprites.Count; j++)
                    {
                        segment.spriteContainer.Sprites[i].UpdateSprite(segment.Players[i].Position_Player);


                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.spriteContainer.Sprites[j].Texture);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(segment.spriteContainer.Sprites[j].SpriteMatrix);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());



                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<EffectVertex>(
                                PrimitiveType.TriangleList, segment.spriteContainer.Sprites[j].spritePlane,
                                0,
                                segment.spriteContainer.Sprites[j].spritePlane.Length / 3);
                        }
                    }
                }
                #endregion

                #region DrawGunSprites
                if (drawGunSprites)
                {
                    /*
                    if (segment.firearmSprites.Sprites != null && segment.firearmSprites.Sprites[i] != null)
                    {
                        RasterizerState s = new RasterizerState();
                        s.CullMode = CullMode.None;
                        GraphicsDevice.BlendState = BlendState.Additive;

                        GraphicsDevice.RasterizerState = s;


                        //segment.firearmSprites.Sprites[i].UpdateSprite(segment.Players[i].Position_Player);


                        segment.Effects[ShaderChocie].CurrentTechnique = segment.Effects[ShaderChocie].Techniques["SimpleTexture"];
                        segment.Effects[ShaderChocie].Parameters["ModelTexture"].SetValue(segment.spriteContainer.Sprites[i].Texture);
                        segment.Effects[ShaderChocie].Parameters["World"].SetValue(segment.firearmSprites.Sprites[i].SpriteMatrix);
                        segment.Effects[ShaderChocie].Parameters["View"].SetValue(segment.Players[i].GetCamera().Get_ViewMatrix());
                        segment.Effects[ShaderChocie].Parameters["Projection"].SetValue(segment.Players[i].GetCamera().Get_Projection());


                        foreach (EffectPass eff in segment.Effects[ShaderChocie].CurrentTechnique.Passes)
                        {
                            eff.Apply();
                            GraphicsDevice.DrawUserPrimitives<EffectVertex>(
                                PrimitiveType.TriangleList, segment.spriteContainer.Sprites[i].spritePlane,
                                0,
                                segment.firearmSprites.Sprites[i].spritePlane.Length / 3);
                        }
                    }
                     * */
                }

                #endregion
                                
            }
        }

        public void UpdatePlayerInput(List<Player> players, GeneralGameData gamedat)
        {
            Microsoft.Xna.Framework.Input.KeyboardState k = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            //Loop throught Gamepads, Mouse and keybaord is always player 1
            for (int i = 0; i < gamedat.PlayerCount; i++)
            {
                if (Segment01.Players[i].playerIsDead) { }

                #region FPS rotation stuff

                Vector3 playerMovement = Vector3.Zero;
                float height = players[i].Get_PlayerPosition().Y;

                if (k.IsKeyDown(Keys.LeftShift))
                    players[i].Walkspeed_Current = players[i].WalkSpeed_Running;
                else if (k.IsKeyDown(Keys.LeftAlt))
                    players[i].Walkspeed_Current = players[i].WalkSpeed_Sneak;
                else
                    players[i].Walkspeed_Current = players[i].WalkSpeed_Walking;

                if (k.IsKeyDown(Keys.W))
                {
                    Vector3 rotatedLookAt = VectorMath.Vector3_Rotate(players[i].GetCamera().Get_LookAt(), players[i].GetCamera().Get_Rotation());
                    float length = VectorMath.get_length(new Vector2(rotatedLookAt.X, rotatedLookAt.Z));

                    playerMovement += VectorMath.Vector3_Rotate(
                         (players[i].Get_PlayerLookat() * players[i].Walkspeed_Current) / length, players[i].Get_PlayerRotation()) / 5;
                    playerMovement.Y = height;
                }
                if (k.IsKeyDown(Keys.S))
                {
                    Vector3 rotatedLookAt = VectorMath.Vector3_Rotate(players[i].GetCamera().Get_LookAt(), players[i].GetCamera().Get_Rotation());
                    float length = VectorMath.get_length(new Vector2(rotatedLookAt.X, rotatedLookAt.Z));

                    playerMovement += VectorMath.Vector3_Rotate(
                       (players[i].Get_PlayerLookat() * players[i].Walkspeed_Current) / length, players[i].Get_PlayerRotation()) / -5;
                    playerMovement.Y = height;
                }

                if (k.IsKeyDown(Keys.A))
                {
                    playerMovement += VectorMath.Vector3_Rotate(
                        VectorMath.Vector3_Rotate(
                            players[i].Get_PlayerLookat() * players[i].Walkspeed_Current, new Vector3(0, MathHelper.PiOver2, 0)), players[i].Get_PlayerRotation()) / -5;
                    playerMovement.Y = height;
                }

                if (k.IsKeyDown(Keys.D))
                {
                    float length = VectorMath.get_length(new Vector2(players[i].Get_PlayerLookat().X, players[i].Get_PlayerLookat().Z));

                    playerMovement += VectorMath.Vector3_Rotate(
                        VectorMath.Vector3_Rotate(
                            (players[i].Get_PlayerLookat() * players[i].Walkspeed_Current) / length, new Vector3(0, MathHelper.PiOver2, 0)), players[i].Get_PlayerRotation()) / 5;
                    playerMovement.Y = height;
                }

                if (k.IsKeyDown(Keys.Q))
                {
                    float roty = -players[i].Get_PlayerRotation().Y + MathHelper.PiOver2;
                    Matrix maty = Matrix.CreateRotationY(roty);
                    Vector3 newRelPos = Vector3.Transform(new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, -1.2f), maty);
                    players[i].GetCamera().CameraRelativePosition_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Final = players[i].GetCamera().CameraUpVector_Adapt + players[i].GetCamera().CameraUpVector_Static;

                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, -0.7f);
                }
                else if (k.IsKeyDown(Keys.E))
                {
                    float roty = -players[i].Get_PlayerRotation().Y + MathHelper.PiOver2;
                    Matrix maty = Matrix.CreateRotationY(roty);
                    Vector3 newRelPos = Vector3.Transform(new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, 1.2f), maty);
                    players[i].GetCamera().CameraRelativePosition_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Final = players[i].GetCamera().CameraUpVector_Adapt + players[i].GetCamera().CameraUpVector_Static;

                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, 0.7f);
                }
                else
                {
                    /*
                    players[i].GetCamera().CameraRelativePosition_Set = new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, 0);
                    players[i].GetCamera().CameraUpVector_Set = Vector3.Up ;
                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, 0);
                    */

                }

                //-------------------------

                CollisionDetection.HandleGameEntityCollision(players[i], Segment01, 0.1f, 1.0f);
                playerMovement.Y = 0;
                players[i].Set_PlayerPosition(Vector3.Add(playerMovement, players[i].Get_PlayerPosition()));
                if (playerMovement != Vector3.Zero)
                {
                    Console.WriteLine("movement len is {0}", VectorMath.get_length(playerMovement));
                }



                Vector2 v = MouseInput.getMouseDifference(gamedat.width, gamedat.height);

                if (gamedat.PlayerCount == 1)
                {
                    Vector3 BreathMovement0 = new Vector3((float)Math.Sin(gamedat.function_counters[0]) / 3, (float)Math.Cos(gamedat.function_counters[0]) / 3, 0) / 2000;

                    players[i].Set_PlayerRotation(Vector3.Add(players[i].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement0);

                }
                else if (gamedat.PlayerCount == 2)
                {
                    Vector3 BreathMovement0 = new Vector3((float)Math.Sin(gamedat.function_counters[0]) / 3, (float)Math.Cos(gamedat.function_counters[0]) / 3, 0) / 2000;
                    Vector3 BreathMovement1 = new Vector3((float)Math.Sin(gamedat.function_counters[1]) / 3, (float)Math.Cos(gamedat.function_counters[1]) / 3, 0) / 2000;
                    players[0].Set_PlayerRotation(Vector3.Add(players[0].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement0);
                    players[1].Set_PlayerRotation(Vector3.Add(players[1].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement1);
                }
                else if (gamedat.PlayerCount == 3)
                {
                    Vector3 BreathMovement0 = new Vector3((float)Math.Sin(gamedat.function_counters[0]) / 3, (float)Math.Cos(gamedat.function_counters[0]) / 3, 0) / 2000;
                    Vector3 BreathMovement1 = new Vector3((float)Math.Sin(gamedat.function_counters[1]) / 3, (float)Math.Cos(gamedat.function_counters[1]) / 3, 0) / 2000;
                    Vector3 BreathMovement2 = new Vector3((float)Math.Sin(gamedat.function_counters[2]) / 3, (float)Math.Cos(gamedat.function_counters[2]) / 3, 0) / 2000;
                    players[0].Set_PlayerRotation(Vector3.Add(players[0].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement0);
                    players[1].Set_PlayerRotation(Vector3.Add(players[1].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement1);
                    players[2].Set_PlayerRotation(Vector3.Add(players[2].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement2);
                }
                else if (gamedat.PlayerCount == 4)
                {
                    Vector3 BreathMovement0 = new Vector3((float)Math.Sin(gamedat.function_counters[0]) / 3, (float)Math.Cos(gamedat.function_counters[0]) / 3, 0) / 2000;
                    Vector3 BreathMovement1 = new Vector3((float)Math.Sin(gamedat.function_counters[1]) / 3, (float)Math.Cos(gamedat.function_counters[1]) / 3, 0) / 2000;
                    Vector3 BreathMovement2 = new Vector3((float)Math.Sin(gamedat.function_counters[2]) / 3, (float)Math.Cos(gamedat.function_counters[2]) / 3, 0) / 2000;
                    Vector3 BreathMovement3 = new Vector3((float)Math.Sin(gamedat.function_counters[3]) / 3, (float)Math.Cos(gamedat.function_counters[3]) / 3, 0) / 2000;
                    players[0].Set_PlayerRotation(Vector3.Add(players[0].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement0);
                    players[1].Set_PlayerRotation(Vector3.Add(players[1].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement1);
                    players[2].Set_PlayerRotation(Vector3.Add(players[2].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement2);
                    players[3].Set_PlayerRotation(Vector3.Add(players[3].Get_PlayerRotation(), new Vector3(v.Y, v.X, 0)) + BreathMovement3);
                }
                #endregion


            }
        }

        public void initialiseJoystick(GameSegment seg, GeneralGameData gamedat)
        {

            seg.joystickinput.initialiseJoysticks();

            for (int i = 0; i < seg.joystickinput.guids.Count && i < seg.Players.Count; i++)
            {
                seg.Players[i].JoyStickGuid = Convert.ToString(seg.joystickinput.guids[i]);
            }
        }

        public void UpdatePlayerInput_joysticks(GameSegment seg, List<Player> players, GeneralGameData gamedat)
        {

            Segment01.joystickinput.Get_JoystickData();

            for (int i = 0; i < gamedat.PlayerCount; i++)
            {
                Vector3 playerMovement = Vector3.Zero;
                float height = players[i].Get_PlayerPosition().Y;
                float movementlen = 0;

                //For Respawn
                if (Segment01.joystickinput.JoystickData[i].Start.Value > 0)
                {
                    if (seg.Players[i].playerRespawnTimer == 0 && seg.Players[i].playerIsDead)
                    {
                        seg.Players[i].playerRespawnTimer = seg.Players[i].playerRespawnTime;
                        seg.Players[i].AdditionalMovement = Vector3.Zero;
                        seg.Players[i].playerIsDead = false;
                        seg.Players[i].Health = 100;
                        seg.Players[i].Armor = 0;
                        Random r = new Random();
                        float num1 = (float)(r.NextDouble() - 0.5f) * 400;
                        float num2 = (float)(r.NextDouble() - 0.5f) * 400;
                        seg.Players[i].Set_PlayerPosition(new Vector3(num1, 90, num2));
                    }
                }

                if (players[i].playerIsDead)
                    continue;

                //For Jumping
                if (Segment01.joystickinput.JoystickData[i].ButtonCross.Value > 0)
                {
                    if (seg.Players[i].CanJump)
                    {
                        seg.Players[i].IsJumpingOrFlying = true;

                        seg.Players[i].Position_Player.Y += 0.3f;

                        seg.Players[i].AdditionalMovement.Y += 0.08f * seg.Players[i].JumpMultiply;
                    }
                }

                if (Segment01.joystickinput.JoystickData[i].ButtonLeft2.Value > 0)
                {
                    seg.Players[i].PlayerCamera.Set_Projection(Matrix.CreatePerspectiveFieldOfView(30 / 57.2f, gamedat.AspectRatio, gamedat.ViewDisMin, gamedat.ViewDisMax));

                }
                else
                {
                    seg.Players[i].PlayerCamera.Set_Projection(Matrix.CreatePerspectiveFieldOfView(60.0f / 57.2f, gamedat.AspectRatio, gamedat.ViewDisMin, gamedat.ViewDisMax));

                    //seg.Players[i].PlayerCamera.Set_Projection(Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver2, gamedat.AspectRatio, gamedat.ViewDisMin, gamedat.ViewDisMax));
                }
                //For jetpack
                if (Segment01.joystickinput.JoystickData[i].ButtonLeft1.Value > 0)
                {
                    if (seg.Players[i].JetpackEnabled)
                    {
                        if (seg.Players[i].AdditionalMovement.Y < 0.001f && seg.Players[i].AdditionalMovement.Y > -0.001f)
                        {
                            seg.Players[i].Position_Player.Y += 0.3f;
                        }
                        else
                            seg.Players[i].Position_Player.Y += 0.3f;

                        seg.Players[i].AdditionalMovement.Y += (0.10f / (seg.Players[i].Position_Player.Y / 10));
                        if (seg.Players[i].AdditionalMovement.Y > 0.05f)
                            seg.Players[i].AdditionalMovement.Y = 0.05f;

                        if (seg.Players[i].AdditionalMovement.Y > 0)
                            seg.Players[i].IsJumpingOrFlying = true;
                    }
                }

                //For Shooting
                if (Segment01.joystickinput.JoystickData[i].ButtonRight2.Value > 0)
                {
                    //((Weapon_Shotgun)seg.Players[i].currentWeapon).FireShot("pistol_sounds",   seg, gamedat);
                }

                if (Segment01.joystickinput.JoystickData[i].ButtonRight2.Value < 10)
                {
                    //((Weapon_Shotgun)seg.Players[i].currentWeapon).trigger_blocked = false;
                }


                Vector2 movement_adjusted = Vector2.Zero;

                //For the Movement
                #region Left stick data
                if (Segment01.joystickinput.JoystickData[i].StickLeftY.RawOffset == 4)
                {
                    float value = 1 - Segment01.joystickinput.JoystickData[i].StickLeftY.Value / (float)short.MaxValue;

                    Vector3 rotatedLookAt = VectorMath.Vector3_Rotate(players[i].GetCamera().Get_LookAt(), players[i].GetCamera().Get_Rotation());
                    float length = VectorMath.get_length(new Vector2(rotatedLookAt.X, rotatedLookAt.Z));

                    playerMovement += VectorMath.Vector3_Rotate(
                         ((players[i].Get_PlayerLookat() * players[i].Walkspeed_Current * value)) / length, players[i].Get_PlayerRotation()) / 5;

                    playerMovement.Y = height;
                    movementlen = VectorMath.get_length(new Vector2(playerMovement.X, playerMovement.Z));
                    if (movementlen > 0.2f)
                    {
                        movementlen = movementlen - (movementlen - 0.2f);
                    }
                    movement_adjusted = Vector2.Normalize(new Vector2(playerMovement.X, playerMovement.Z));
                    movement_adjusted *= 2 * movementlen;
                }


                if (Segment01.joystickinput.JoystickData[i].StickLeftX.RawOffset == 0)
                {
                    float value = 1 - Segment01.joystickinput.JoystickData[i].StickLeftX.Value / (float)short.MaxValue;

                    Vector3 rotatedLookAt = VectorMath.Vector3_Rotate(players[i].GetCamera().Get_LookAt(), players[i].GetCamera().Get_Rotation());
                    float length = VectorMath.get_length(new Vector2(rotatedLookAt.X, rotatedLookAt.Z));

                    playerMovement += 2 * VectorMath.Vector3_Rotate(
                         ((players[i].Get_PlayerLookat() * players[i].Walkspeed_Current * value)) / length, players[i].Get_PlayerRotation() + new Vector3(0, -MathHelper.PiOver2, 0)) / 5;

                    playerMovement.Y = height;
                    movementlen = VectorMath.get_length(new Vector2(playerMovement.X, playerMovement.Z));
                    if (movementlen > 0.2f)
                    {
                        movementlen = movementlen - (movementlen - 0.2f);
                    }
                    movement_adjusted = Vector2.Normalize(new Vector2(playerMovement.X, playerMovement.Z));
                    movement_adjusted *= movementlen;
                }

                //For the left-right panning of the players view/Animation matrix
                if (Segment01.joystickinput.JoystickData[i].ButtonLeft1.Value > 0)
                {
                    float roty = -players[i].Get_PlayerRotation().Y + MathHelper.PiOver2;
                    Matrix maty = Matrix.CreateRotationY(roty);
                    Vector3 newRelPos = Vector3.Transform(new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, -1.2f), maty);
                    players[i].GetCamera().CameraRelativePosition_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Final = players[i].GetCamera().CameraUpVector_Adapt + players[i].GetCamera().CameraUpVector_Static;

                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, -0.7f);
                }
                else if (Segment01.joystickinput.JoystickData[i].ButtonRight1.Value > 0)
                {
                    float roty = -players[i].Get_PlayerRotation().Y + MathHelper.PiOver2;
                    Matrix maty = Matrix.CreateRotationY(roty);
                    Vector3 newRelPos = Vector3.Transform(new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, 1.2f), maty);
                    players[i].GetCamera().CameraRelativePosition_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Set = newRelPos;
                    players[i].GetCamera().CameraUpVector_Final = players[i].GetCamera().CameraUpVector_Adapt + players[i].GetCamera().CameraUpVector_Static;

                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, 0.7f);
                }
                else
                {
                    players[i].GetCamera().CameraRelativePosition_Set = new Vector3(0, players[i].GetCamera().CameraRelativePosition_Set.Y, 0);
                    players[i].GetCamera().CameraUpVector_Set = Vector3.Up;
                    //For anim
                    players[i].LeftRightBend_Set = new Vector3(0, 0, 0);
                }
                //--------------------------------------------------------------------------------------

                if (movement_adjusted.X > -0.2f && movement_adjusted.X < 0.2f)
                {
                    playerMovement.X = movement_adjusted.X;
                }
                else
                    playerMovement.X = 0;

                if (movement_adjusted.Y > -0.2f && movement_adjusted.Y < 0.2f)
                {
                    playerMovement.Z = movement_adjusted.Y;
                }
                else
                    playerMovement.Z = 0;
                #endregion

                //Movement for now
                seg.Players[i].CurrentPlayMoveDirection = playerMovement;
                playerMovement.Y = 0;
                //-------------------------
                players[i].Set_PlayerPosition(Vector3.Add(playerMovement, players[i].Get_PlayerPosition()));
                //----------------------------------//

                if (Segment01.joystickinput.JoystickData[i].StickRightX.RawOffset == 20)
                {
                    float value = (1 - Segment01.joystickinput.JoystickData[i].StickRightX.Value / (float)short.MaxValue);
                    value *= -0.02f;
                    if (!(value > -1 && value < 1))
                    {
                        value = 0;
                    }

                    players[i].Set_PlayerRotation(Vector3.Add(players[i].Get_PlayerRotation(), 2 * new Vector3(0, value, 0)));
                }

                if (Segment01.joystickinput.JoystickData[i].StickRightY.RawOffset == 24)
                {
                    float value = (1 - Segment01.joystickinput.JoystickData[i].StickRightY.Value / (float)short.MaxValue);
                    value *= -0.02f;
                    if (!(value > -1 && value < 1))
                    {
                        value = 0;
                    }

                    players[i].Set_PlayerRotation(Vector3.Add(players[i].Get_PlayerRotation(), 2 * new Vector3(value, 0, 0)));
                }
            }
        }

        //CHANGED SOME IMPORTANT STUFF HERE
        public void UpdatePlayerAndCamera(List<Player> Players, GeneralGameData gamedat)
        {
            for (int i = 0; i < gamedat.PlayerCount; i++)
            {
                if (i == 5)
                {
                    Players[i].updateViewMatrix();
                    //FOR NOW player Position IS camera position
                    Players[i].PlayerCamera.Set_Rotation(new Vector3(0, MathHelper.PiOver4, 0));
                    Players[i].PlayerCamera.Set_Position(new Vector3(0, 5, 30));
                    Players[i].GetCamera().Set_Projection(Segment01.Perspectives[0]);
                    Players[i].GetCamera().CameraUpVector_Final = Vector3.Up;
                    Players[i].PlayerCamera.updateViewMatrix();
                    Players[i].PlayerCamera.updateCameraLookTowards(); //For guns/such
                    return;
                }


                if (Segment01.Players[i].playerIsDead) { }

                Players[i].updateViewMatrix();
                //FOR NOW player Position IS camera position
                Players[i].PlayerCamera.Set_Rotation(Players[i].Get_PlayerRotation());
                Players[i].PlayerCamera.Set_Position(Players[i].Get_PlayerPosition());


                Matrix view = Matrix.CreateLookAt(Players[i].GetCamera().Get_Positon(), Players[i].GetCamera().CameraLookForwardDirection, new Vector3(0, 1, 0.2f));
                Players[i].GetCamera().Set_ViewMatrix(view);
                Players[i].PlayerCamera.Set_Position(Players[i].Get_PlayerPosition() + Players[i].GetCamera().CameraRelativePosition_Adapt);
                Players[i].PlayerCamera.CameraUpVector_Final = Players[i].GetCamera().CameraUpVector_Static + Players[i].GetCamera().CameraUpVector_Adapt;
                Players[i].PlayerCamera.updateViewMatrix();
                Players[i].PlayerCamera.updateCameraLookTowards(); //For guns/such
            }
        }

        public void UpdateItems(GameSegment seg, GeneralGameData dat)
        {
            for (int i = 0; i < seg.itemsInMap.Count; i++)
            {
                seg.itemsInMap[i].UpdateItem();
                seg.itemsInMap[i].UpdateCollider();
            }
        }

        public void UpdatePlayerStuff(GameSegment seg, GeneralGameData dat)
        {
            for (int i = 0; i < dat.PlayerCount; i++)
            {
                seg.Players[i].UpdatePlayerStuff();
                seg.Players[i].updateConvexHull();
            }
        }

            #endregion

        /* THOSE Function FOLLOWING ARE FOR THE MENU ONLY */
        #region Menu Functions

        public void Create_MenuRenderWindow_Players(MenuSegment menu, GeneralGameData dat)
        {
            Console.WriteLine("Creating Rendertargets for menus");

            menu.previewRenderTargets = new RenderTarget2D[4];
            menu.previewRenderTargets[0] = new RenderTarget2D(GraphicsDevice, 400, 400, true, SurfaceFormat.Color, DepthFormat.Depth16);
            menu.previewRenderTargets[1] = new RenderTarget2D(GraphicsDevice, 400, 400, true, SurfaceFormat.Color, DepthFormat.Depth16);

            menu.viewports_menu = new Viewport[4];
            menu.viewports_menu[0] = new Viewport(0, 0, dat.width / 2, dat.height);
            menu.viewports_menu[1] = new Viewport(dat.width / 2, 0, dat.width / 2, dat.height);
        }

        public void Load_MenuModels(MenuSegment menu, GeneralGameData dat)
        {
            /*
            menu.previewModels[0] = Animation_Functions.LoadAnimatedMeshFromFBX(dat, "Models//Physmodel_textured.fbx");
            menu.previewModels[0].AnimationController = new AnimationController(null, string.Empty);
            menu.previewModels[0].AnimationController.ControllerOwnerName = "PlayerIdle";
            menu.previewModels[0].AnimationController.AnimationContainer = Animation_Functions.LoadAnimationsFromFile(dat, "Animation//Animation_Dance.txt",1); 

            menu.previewModels[1] = Animation_Functions.LoadAnimatedMeshFromFBX(dat, "Models//Physmodel_textured.fbx");
            menu.previewModels[1].AnimationController = new AnimationController(null, string.Empty);
            menu.previewModels[1].AnimationController.ControllerOwnerName = "PlayerIdle";
            menu.previewModels[1].AnimationController.AnimationContainer = Animation_Functions.LoadAnimationsFromFile(dat, "Animation//Animation_Dance.txt",1); 


            #region Get Animation Length
            for (int i = 0; i < menu.previewModels[0].AnimationController.AnimationContainer.AnimationCollection.Count; i++)
            {
                if (menu.previewModels[0].AnimationController.AnimationContainer.AnimationCollection[i].Name.Equals("Dance"))
                {
                    menu.previewModels[0].AnimationController.AnimationLength = menu.previewModels[0].AnimationController.AnimationContainer.AnimationCollection[i].AnimationFrames.Count - 2;
                }
            }
            for (int i = 0; i < menu.previewModels[1].AnimationController.AnimationContainer.AnimationCollection.Count; i++)
            {
                if (menu.previewModels[1].AnimationController.AnimationContainer.AnimationCollection[i].Name.Equals("Dance"))
                {
                    menu.previewModels[1].AnimationController.AnimationLength = menu.previewModels[1].AnimationController.AnimationContainer.AnimationCollection[i].AnimationFrames.Count - 2;
                }
            }
            #endregion

            Stream s = new FileStream(menu.previewModels[0].textureNames[0], FileMode.Open);
            menu.previewModels[0].DiffuseTexture = Texture2D.FromStream(this.GraphicsDevice, s);
            s.Close();

            s = new FileStream(menu.previewModels[1].textureNames[0], FileMode.Open);
            menu.previewModels[1].DiffuseTexture = Texture2D.FromStream(this.GraphicsDevice, s);
            s.Close();

            menu.previewModels[0].AnimationController.AnimationTimer = 0;
            menu.previewModels[0].AnimationController.AnimationEnded = true;
            menu.previewModels[0].AnimationController.AnimationRunning = false;
        
            menu.previewModels[1].AnimationController.AnimationTimer = 0;
            menu.previewModels[1].AnimationController.AnimationEnded = true;
            menu.previewModels[1].AnimationController.AnimationRunning = false;
           */
        }

        public void Menu_Draw_PlayerModels(MenuSegment menu, GeneralGameData dat)
        {
            this.menuSegment.previewModels[0].base_rotation_optional.Y += 0.015f;
            this.menuSegment.previewModels[1].base_rotation_optional.Y += 0.015f;
            this.GraphicsDevice.Viewport = menu.viewports_menu[0];
            //-------------------------------------------------------------------------------     
            menu.Render_Effect.CurrentTechnique = menu.Render_Effect.Techniques["SimpleTexture"];
            menu.Render_Effect.Parameters["View"].SetValue(Matrix.CreateLookAt(new Vector3(0, 4, -10), new Vector3(0, 3, 0), Vector3.Up));
            menu.Render_Effect.Parameters["Projection"].SetValue(Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 1, 0.1f, 100));
            menu.Render_Effect.Parameters["Transparency"].SetValue(1);
            menu.Render_Effect.Parameters["DiffuseLightDirection"].SetValue(dat.DiffuseLightDirection);
            menu.Render_Effect.Parameters["DiffuseLightColor"].SetValue(dat.DiffuseLightColor);
            menu.Render_Effect.Parameters["DiffuseLightIntensity"].SetValue(dat.DiffuseLightIntensity);
            menu.Render_Effect.Parameters["SpecularReflectivity"].SetValue(dat.SpecularReflectivity);
            menu.Render_Effect.Parameters["AmbientLightAndIntensity"].SetValue(dat.AmbientLightAndIntensity);

            //Render Model 1
            menu.Render_Effect.Parameters["World"].SetValue(Matrix.CreateRotationY(menuSegment.previewModels[0].base_rotation_optional.Y));
            menu.Render_Effect.Parameters["ModelTexture"].SetValue(menu.previewModels[0].DiffuseTexture);
            GraphicsDevice.SetRenderTarget(menu.previewRenderTargets[0]);
            GraphicsDevice.Clear(Color.Black);
            RasterizerState rastwer = new RasterizerState();
            rastwer.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rastwer;
            foreach (EffectPass eff in menu.Render_Effect.CurrentTechnique.Passes)
            {
                eff.Apply();

                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                    PrimitiveType.TriangleList,
                    menu.previewModels[0].Vertices,
                    0,
                     menu.previewModels[0].Vertices.Length / 3);
            }


            //Render Model 2
            menu.Render_Effect.Parameters["World"].SetValue(Matrix.CreateRotationY(menuSegment.previewModels[1].base_rotation_optional.Y));
            menu.Render_Effect.Parameters["ModelTexture"].SetValue(menu.previewModels[1].DiffuseTexture);
            GraphicsDevice.SetRenderTarget(menu.previewRenderTargets[1]);
            GraphicsDevice.Clear(Color.Black);
            rastwer = new RasterizerState();
            rastwer.CullMode = CullMode.CullCounterClockwiseFace;
            GraphicsDevice.RasterizerState = rastwer;
            foreach (EffectPass eff in menu.Render_Effect.CurrentTechnique.Passes)
            {
                eff.Apply();

                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                    PrimitiveType.TriangleList,
                    menu.previewModels[1].Vertices,
                    0,
                     menu.previewModels[1].Vertices.Length / 3);
            }
        }

        public void UpdateAnimationData_Menu()
        {
            /*
            if (menuSegment.previewModels[0].AnimationController.AnimationTimer < menuSegment.previewModels[0].AnimationController.AnimationLength)
            {
                menuSegment.previewModels[0].AnimationController.AnimationTimer++;

                Animation_Functions.PlayAnimation("Dance",    menuSegment.previewModels[0], Matrix.Identity);
            }
            else
                menuSegment.previewModels[0].AnimationController.AnimationTimer = 0;

            if (menuSegment.previewModels[1].AnimationController.AnimationTimer < menuSegment.previewModels[1].AnimationController.AnimationLength)
            {
                menuSegment.previewModels[1].AnimationController.AnimationTimer++;

                Animation_Functions.PlayAnimation("Dance",    menuSegment.previewModels[1], Matrix.Identity);
            }
            else
                menuSegment.previewModels[1].AnimationController.AnimationTimer = 0;
            */
        }

        public void Load_basicMenuData(MenuSegment menu, GeneralGameData dat)
        {
            //Setup
            menu.Effect = new BasicEffect(GraphicsDevice);
            menu.Render_Effect = Content.Load<Microsoft.Xna.Framework.Graphics.Effect>("Content//Phong");
            menu.fonts.Add(Content.Load<SpriteFont>("Content//MenuFont_Large"));
            menu.fonts.Add(Content.Load<SpriteFont>("Content//MenuFont_Medium"));
            menu.fonts.Add(Content.Load<SpriteFont>("Content//MenuFont_Small"));
            menu.spritebatch = new SpriteBatch(GraphicsDevice);

            //Now textures
            FileStream s;
            s = File.Open(gameData.GameDirectory + "//Gui//Game_Background.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Background_wide.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Menu_Box_wide.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Menu_Box_wide_clicked.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Menu_Element_wide.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Menu_Element_wide_clicked.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//Rendertarget.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//LevelImage.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();

            s = File.Open(gameData.GameDirectory + "//Gui//plain_blue.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();
            s = File.Open(gameData.GameDirectory + "//Gui//plain_blue_clicked.png", FileMode.Open);
            menu.menu_textures.Add(Texture2D.FromStream(GraphicsDevice, s));
            s.Close();
        }

        public void Create_MenuPages(MenuSegment menu, GeneralGameData dat)
        {
            menu.currentPageIndex = 42;
            //------------------------------------------------------------------------------------------//
            //First Page - Entry screen
            #region Intro Page
            MenuPage page01_intro = new MenuPage("Entry Screen", 0);

            // Background image
            MenuElement page01_background = new MenuElement(0, -1,
                new Point(dat.width, dat.height), menu.Effect, "", Vector2.Zero,
                new Rectangle(0, 0, dat.width, dat.height), false);

            //Button Element
            MenuElement page01_introfield;
            page01_introfield = new MenuElement(1, 3,
                new Point(dat.width, dat.height), menu.Effect, "Press on this\nField to continue.",
                new Vector2((float)dat.width / 2 - 120, (float)dat.height / 2 - 40),
                new Rectangle((int)(3.5f * (float)dat.width / 10), (int)(3.5f * (float)dat.height / 10), dat.width - 7 * (dat.width / 10),
                    dat.height - 7 * (dat.height / 10)), true);

            //Button Element
            MenuElement page01_Quit;
            page01_Quit = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Quit",
                new Vector2(
                    (int)(17.3f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)(16.5f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.0f),
                    (int)((float)dat.width / 6f),
                    dat.height / 13),
                    true);
            #endregion
            //------------------------------------------------------------------------------------------//

            //------------------------------------------------------------------------------------------//
            //Second Page - Menu Page
            #region Player Number Page
            MenuPage page02_playerNumber = new MenuPage("Choose Player", 1);

            // Background image
            MenuElement page02_background = new MenuElement(0, -1,
                new Point(dat.width, dat.height), menu.Effect, "", Vector2.Zero,
                new Rectangle(0, 0, dat.width, dat.height), false);

            //Button Background
            MenuElement page02_playerButtonBackground;
            page02_playerButtonBackground = new MenuElement(2, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2((float)dat.width / 2 - 120, (float)dat.height / 2 - 40),
                new Rectangle(
                    (int)(3.3f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 7.5f),
                    dat.width - 2 * (int)(3.38f * (float)dat.width / 10),
                    dat.height - 2 * (int)((float)(dat.height / 20) * 7.5f)),
                    false);

            //Button Element
            MenuElement page02_player1;
            page02_player1 = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "1 Player",
                new Vector2((float)dat.width / 20 * 8.5f, (int)((float)(dat.height / 20) * 8.25f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 7.8f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            //Button Element
            MenuElement page02_player2;
            page02_player2 = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "2 Player",
                new Vector2((float)dat.width / 20 * 8.5f, (int)((float)(dat.height / 20) * 10.6f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 10.2f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            //Button Element
            MenuElement page02_back;
            page02_back = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Back",
                new Vector2(
                    (int)(17.3f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)(16.5f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.0f),
                    (int)((float)dat.width / 6f),
                    dat.height / 13),
                    true);

            #endregion
            //------------------------------------------------------------------------------------------//

            //------------------------------------------------------------------------------------------//
            //Third Page - Play Mode
            #region Mode -> Arcade , Story, MapEditor
            MenuPage page03_PickMode = new MenuPage("Pick Mode", 2);

            // Background image
            MenuElement page03_Background = new MenuElement(0, -1,
                new Point(dat.width, dat.height), menu.Effect, "", Vector2.Zero,
                new Rectangle(0, 0, dat.width, dat.height), false);

            //Button Background
            MenuElement page03_ButtonBackground;
            page03_ButtonBackground = new MenuElement(2, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2((float)dat.width / 2 - 120, (float)dat.height / 2 - 40),
                new Rectangle(
                    (int)(3.3f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 7.5f),
                    dat.width - 2 * (int)(3.38f * (float)dat.width / 10),
                    dat.height - 2 * (int)((float)(dat.height / 20) * 6.32f)),
                    false);

            //Button Element
            MenuElement page03_modeStory;
            page03_modeStory = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "StoryMode",
                new Vector2((float)dat.width / 20 * 8.3f, (int)((float)(dat.height / 20) * 8.25f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 7.8f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            //Button Element
            MenuElement page03_modeArcade;
            page03_modeArcade = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "ArcadeMode",
                new Vector2((float)dat.width / 20 * 8.3f, (int)((float)(dat.height / 20) * 10.6f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 10.2f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            MenuElement page03_modeEditor;
            page03_modeEditor = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "MapmakerMode",
                new Vector2((float)dat.width / 20 * 7.7f, (int)((float)(dat.height / 20) * 13f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 12.6f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            //Button Element
            MenuElement page03_back;
            page03_back = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Back",
                new Vector2(
                    (int)(17.3f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)(16.5f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.0f),
                    (int)((float)dat.width / 6f),
                    dat.height / 13),
                    true);

            #endregion
            //------------------------------------------------------------------------------------------//

            //------------------------------------------------------------------------------------------//
            //Game Modes for Arcade
            #region Arcade game modes
            MenuPage page04_B_PickGameMode = new MenuPage("Pick Game Mode", 32); // 32 as in Page 3 Path 2

            // Background image
            MenuElement page04_B_Background = new MenuElement(0, -1,
                new Point(dat.width, dat.height), menu.Effect, "", Vector2.Zero,
                new Rectangle(0, 0, dat.width, dat.height), false);

            //Button Background
            MenuElement page04_B_ButtonBackground;
            page04_B_ButtonBackground = new MenuElement(2, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2((float)dat.width / 2 - 120, (float)dat.height / 2 - 40),
                new Rectangle(
                    (int)(3.3f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 2.9f),
                    dat.width - 2 * (int)(3.38f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 2.9f * 5f)),
                    false);

            //Return to last menu
            MenuElement page04_B_Back;
            page04_B_Back = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Back",
                new Vector2(
                    (int)(17.3f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)(16.5f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.0f),
                    (int)((float)dat.width / 6f),
                    dat.height / 13),
                    true);


            //Button Element
            MenuElement page04_B_FreeRoam;
            page04_B_FreeRoam = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Freeroam",
                new Vector2((float)dat.width / 20 * 8.5f, (int)((float)(dat.height / 20) * 3.65f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 3.2f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            //Button Element
            MenuElement page04_B_Deathmatch;
            page04_B_Deathmatch = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Deathmatch",
                new Vector2((float)dat.width / 20 * 8.3f, (int)((float)(dat.height / 20) * 6f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 5.6f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            MenuElement page04_B_TeamDeathmatch;
            page04_B_TeamDeathmatch = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "TeamDeathmatch",
                new Vector2((float)dat.width / 20 * 7.4f, (int)((float)(dat.height / 20) * 8.25f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 7.8f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            MenuElement page04_B_Hunter;
            page04_B_Hunter = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Hunter",
                new Vector2((float)dat.width / 20 * 8.9f, (int)((float)(dat.height / 20) * 10.6f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 10.2f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);

            MenuElement page04_B_IDKnow;
            page04_B_IDKnow = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "SomeModeOK?",
                new Vector2((float)dat.width / 20 * 7.7f, (int)((float)(dat.height / 20) * 13f)),
                new Rectangle(
                    (int)(3.5f * (float)dat.width / 10),
                    (int)((float)(dat.height / 20) * 12.6f),
                    (int)((float)dat.width / 3.5f),
                    dat.height / 10),
                    true);


            #endregion
            //------------------------------------------------------------------------------------------//

            //------------------------------------------------------------------------------------------//
            //Game Mode Details
            #region Set Gamemode details
            MenuPage page05_B_GameModeDetails = new MenuPage("Pick Game Mode", 42); // 32 as in Page 3 Path 2

            // Background image
            MenuElement page05_B_Background = new MenuElement(0, -1,
                new Point(dat.width, dat.height), menu.Effect, "", Vector2.Zero,
                new Rectangle(0, 0, dat.width, dat.height), false);

            //Return to last menu
            MenuElement page05_B_Back;
            page05_B_Back = new MenuElement(4, 5,
                new Point(dat.width, dat.height), menu.Effect, "Back",
                new Vector2(
                    (int)(17.3f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)(16.5f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.0f),
                    (int)((float)dat.width / 6f),
                    dat.height / 13),
                    true);

            MenuElement page05_B_GamemodeSettings;
            page05_B_GamemodeSettings = new MenuElement(1, -1,
                new Point(dat.width, dat.height), menu.Effect, "Settings",
                new Vector2(
                    (int)(1.0f * (float)dat.width / 60),
                    (int)((float)(dat.height / 20) * 17.9f)
                    ),
                new Rectangle(
                    (int)(1 * (float)dat.width / 100),
                    (int)((float)(dat.height / 10) * 1.5f),
                    (int)((float)dat.width / 1.50f),
                    (int)((float)dat.height / 1.35f)),
                    true);

            MenuElement page05_B_TopRenderTarget;
            page05_B_TopRenderTarget = new MenuElement(6, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2(
                    (int)(1.0f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)((float)dat.width / 1.46f),
                    (int)((float)(dat.height / 10) * 1.5f),
                    (int)((float)dat.width / 3.3f),
                    (int)((float)dat.height / 2.70f)),
                    false);

            MenuElement page05_B_BottomRenderTarget;
            page05_B_BottomRenderTarget = new MenuElement(6, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2(
                    (int)(1.0f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)((float)dat.width / 1.46f),
                    (int)((float)(dat.height / 10) * 1.5f) + (int)((float)dat.height / 2.7f),
                    (int)((float)dat.width / 3.30f),
                    (int)((float)dat.height / 2.7f)),
                    false);

            MenuElement page05_B_RenderWindowBackground;
            page05_B_RenderWindowBackground = new MenuElement(7, -1,
                new Point(dat.width, dat.height), menu.Effect, "",
                new Vector2(
                    (int)(1.0f * (float)dat.width / 20),
                    (int)((float)(dat.height / 20) * 18.2f)
                    ),
                new Rectangle(
                    (int)((float)dat.width / 2.37f),
                    (int)((float)(dat.height / 100) * 19f),
                    (int)((float)dat.width / 4.1f),
                    (int)((float)dat.height / 2.7f)),
                    true);

            int setting_box_distance = (int)((float)(dat.height / 13));
            int setting_text_align = (int)(1.0f * (float)dat.width / 100 * 2.5f);

            MenuElement page05_B_Setting1; //element 7 
            page05_B_Setting1 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 01 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 0
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 0,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting1.fontChoice = 2;

            MenuElement page05_B_Setting2; //element 7 
            page05_B_Setting2 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 02 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 1
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 1,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting2.fontChoice = 2;

            MenuElement page05_B_Setting3; //element 7 
            page05_B_Setting3 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 03 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 2
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 2,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting3.fontChoice = 2;

            MenuElement page05_B_Setting4; //element 7 
            page05_B_Setting4 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 04 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 3
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 3,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting4.fontChoice = 2;

            MenuElement page05_B_Setting5; //element 7 
            page05_B_Setting5 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 05 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 4
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 4,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting5.fontChoice = 2;

            MenuElement page05_B_Setting6; //element 7 
            page05_B_Setting6 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 06 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 5
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 5,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting6.fontChoice = 2;

            MenuElement page05_B_Setting7; //element 7 
            page05_B_Setting7 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 07 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 6
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 6,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting7.fontChoice = 2;

            MenuElement page05_B_Setting8; //element 7 
            page05_B_Setting8 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 08 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 7
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 7,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting8.fontChoice = 2;

            MenuElement page05_B_Setting9; //element 7 
            page05_B_Setting9 = new MenuElement(8, 9,
                new Point(dat.width, dat.height), menu.Effect, "Setting 09 is here ",
                new Vector2(
                    setting_text_align,
                    (int)((float)(dat.height / 100) * 20.1f) + setting_box_distance * 8
                    ),
                new Rectangle(
                    (int)((float)(dat.width / 100f) * 2),
                    (int)((float)(dat.height / 100) * 19f) + setting_box_distance * 8,
                    (int)((float)(dat.width / 100) * 40),
                    (int)((float)dat.height / 100) * 7),
                    true);
            page05_B_Setting9.fontChoice = 2;

            #endregion
            //------------------------------------------------------------------------------------------//

            #region Add MenuElements to their respective pages

            //Page 01       PageIndex 0
            page01_intro.menuElements.Add(page01_background);
            page01_intro.menuElements.Add(page01_introfield);
            page01_intro.menuElements.Add(page01_Quit);
            menuSegment.menuPages.Add(page01_intro);

            //Page 02       PageIndex 1
            page02_playerNumber.menuElements.Add(page02_background);
            page02_playerNumber.menuElements.Add(page02_playerButtonBackground);
            page02_playerNumber.menuElements.Add(page02_player1);
            page02_playerNumber.menuElements.Add(page02_player2);
            page02_playerNumber.menuElements.Add(page02_back);
            menuSegment.menuPages.Add(page02_playerNumber);

            //Page 03       PageIndex 2
            page03_PickMode.menuElements.Add(page03_Background);
            page03_PickMode.menuElements.Add(page03_ButtonBackground);

            page03_PickMode.menuElements.Add(page03_modeStory);
            page03_PickMode.menuElements.Add(page03_modeArcade);
            page03_PickMode.menuElements.Add(page03_modeEditor);
            page03_PickMode.menuElements.Add(page03_back);
            menuSegment.menuPages.Add(page03_PickMode);

            //Page 04-B  Pick Arcade Mode  pageIndex 32
            page04_B_PickGameMode.menuElements.Add(page04_B_Background);
            page04_B_PickGameMode.menuElements.Add(page04_B_ButtonBackground);
            page04_B_PickGameMode.menuElements.Add(page04_B_Back);

            page04_B_PickGameMode.menuElements.Add(page04_B_FreeRoam);
            page04_B_PickGameMode.menuElements.Add(page04_B_Deathmatch);
            page04_B_PickGameMode.menuElements.Add(page04_B_TeamDeathmatch);
            page04_B_PickGameMode.menuElements.Add(page04_B_Hunter);
            page04_B_PickGameMode.menuElements.Add(page04_B_IDKnow);
            menuSegment.menuPages.Add(page04_B_PickGameMode);

            //Page Set game mode details  pageIndex 42
            page05_B_GameModeDetails.menuElements.Add(page05_B_Background);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Back);
            page05_B_GameModeDetails.menuElements.Add(page05_B_GamemodeSettings);
            page05_B_GameModeDetails.menuElements.Add(page05_B_TopRenderTarget);
            page05_B_GameModeDetails.menuElements.Add(page05_B_BottomRenderTarget);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting1);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting2);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting3);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting4);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting5);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting6);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting7);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting8);
            page05_B_GameModeDetails.menuElements.Add(page05_B_Setting9);
            page05_B_GameModeDetails.menuElements.Add(page05_B_RenderWindowBackground);
            page05_B_GameModeDetails.menuElements.Add(page05_B_RenderWindowBackground);
            #endregion

            menuSegment.menuPages.Add(page05_B_GameModeDetails);
        }

        public void Render_Preview_Windows(MenuSegment menu)
        {
            GraphicsDevice.SetRenderTarget(null);

            BlendState b = new BlendState();
            b.AlphaBlendFunction = BlendFunction.Add;
            RasterizerState r = new RasterizerState();


            if (menu.currentPageIndex != 42)
                return;

            menu.Effect.Texture = menu.previewRenderTargets[0];
            //Draw the image
            foreach (EffectPass eff in menu.Effect.CurrentTechnique.Passes)
            {
                eff.Apply();

                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                    PrimitiveType.TriangleList, menu.menuPages[4].menuElements[3].polygon.getWindowPlane(),
                    0,
                    2);
            }

            menu.Effect.Texture = menu.previewRenderTargets[1];

            foreach (EffectPass eff in menu.Effect.CurrentTechnique.Passes)
            {
                eff.Apply();

                GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                    PrimitiveType.TriangleList, menu.menuPages[4].menuElements[4].polygon.getWindowPlane(),
                    0,
                    2);
            }
        }

        public void DrawMenuWindows(MenuSegment menu)
        {
            GraphicsDevice.SetRenderTarget(null);
            if (menu.menuPages.Count <= 0)
                return;

            for (int i = 0; i < menu.menuPages.Count; i++)
            {
                if (menu.currentPageIndex != menu.menuPages[i].pageIndex)
                    continue;

                for (int j = 0; j < menu.menuPages[i].menuElements.Count; j++)
                {
                    if (menu.menuPages[i].menuElements[j] == null)
                        continue;

                    //If theres no clicked texture, ignore
                    if (menuSegment.menuPages[i].menuElements[j].currentTexturIndex > -1)
                        menu.Effect.Texture = menuSegment.menu_textures[menuSegment.menuPages[i].menuElements[j].currentTexturIndex];

                    RasterizerState rastwer = new RasterizerState();
                    rastwer.CullMode = CullMode.None;
                    GraphicsDevice.RasterizerState = rastwer;


                    //Draw the image
                    foreach (EffectPass eff in menu.Effect.CurrentTechnique.Passes)
                    {
                        eff.Apply();
                        GraphicsDevice.DrawUserPrimitives<CustomVertex>(
                            PrimitiveType.TriangleList, menu.menuPages[i].menuElements[j].polygon.getWindowPlane(),
                            0,
                            2);
                    }

                    //continue;
                    menu.spritebatch.Begin();
                    menu.spritebatch.DrawString(
                        menu.fonts[menu.menuPages[i].menuElements[j].fontChoice],
                        menu.menuPages[i].menuElements[j].menuText,
                        menu.menuPages[i].menuElements[j].textPosition,
                        Color.White);
                    menu.spritebatch.End();

                    rastwer = new RasterizerState();
                    rastwer.CullMode = CullMode.None;
                    GraphicsDevice.RasterizerState = rastwer;

                }
            }
        }

        public void UpdateMenuFunction(MenuSegment menu)
        {
            if (menu.menuInactiveTimer > 0)
                menu.menuInactiveTimer--;

            if (menu.menuPages.Count < 0)
                return;

            for (int i = 0; i < menu.menuPages.Count; i++)
            {
                //If its not the currentIndex, then ignore
                if (menu.currentPageIndex != menu.menuPages[i].pageIndex)
                    continue;

                //Now take the input for each page

                //ENTRY SCREEN
                #region Take Input for Page 0
                if (menu.menuPages[0].menuElements[1].menuClickedLeft() && menu.currentPageIndex == 0 && menu.menuInactiveTimer == 0)
                {
                    menu.currentPageIndex = 1;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[0].menuElements[2].menuClickedLeft() && menu.currentPageIndex == 0 && menu.menuInactiveTimer == 0)
                {
                    this.Exit();
                }
                #endregion

                //PLAYER COUNT
                #region Take Input for Page 1
                if (menu.menuPages[1].menuElements[2].menuClickedLeft() && menu.currentPageIndex == 1 && menu.menuInactiveTimer == 0)
                {
                    menu.currentPageIndex = 2;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[1].menuElements[3].menuClickedLeft() && menu.currentPageIndex == 1 && menu.menuInactiveTimer == 0)
                {
                    menu.currentPageIndex = 2;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[1].menuElements[4].menuClickedLeft() && menu.currentPageIndex == 1 && menu.menuInactiveTimer == 0)
                {
                    menu.currentPageIndex = 0;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                #endregion

                // STORY ; ARCADE ; MAPMAKER
                #region Take Input for Page 2
                if (menu.menuPages[2].menuElements[2].menuClickedLeft() && menu.currentPageIndex == 2 && menu.menuInactiveTimer == 0)
                {
                    //GO TO STORY
                    Console.WriteLine("Go to story");
                    menu.currentPageIndex = 2;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[2].menuElements[3].menuClickedLeft() && menu.currentPageIndex == 2 && menu.menuInactiveTimer == 0)
                {
                    //ARCADE
                    Console.WriteLine("Go to Arcade");
                    menu.currentPageIndex = 32;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[2].menuElements[4].menuClickedLeft() && menu.currentPageIndex == 2 && menu.menuInactiveTimer == 0)
                {
                    //Go to mapmaker
                    Console.WriteLine("Go to Mapmaker");
                    menu.currentPageIndex = 2;
                    menu.menuInactiveTimer = 50;
                    break;
                }

                if (menu.menuPages[2].menuElements[5].menuClickedLeft() && menu.currentPageIndex == 2 && menu.menuInactiveTimer == 0)
                {
                    //back to page 
                    Console.WriteLine("Go back");
                    menu.currentPageIndex = 1;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                #endregion


                //PICK ARCADE GAME MODE
                #region Take Input for Page 32
                if (menu.menuPages[3].menuElements[2].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Go back to page 2
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 2;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[3].menuElements[3].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Some mode
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 42; //Stay
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[3].menuElements[4].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 42;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[3].menuElements[5].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 42; //Stay 
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[3].menuElements[6].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 42; //Stay 
                    menu.menuInactiveTimer = 50;
                    break;
                }
                if (menu.menuPages[3].menuElements[7].menuClickedLeft() && menu.currentPageIndex == 32 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    Console.WriteLine("A gamemode was picked");
                    menu.currentPageIndex = 42; //Stay 
                    menu.menuInactiveTimer = 50;
                    break;
                }
                #endregion

                #region Take Input for Page 42
                if (menu.menuPages[4].menuElements[1].menuClickedLeft() && menu.currentPageIndex == 42 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    //Console.WriteLine("Oh ma gusch");
                    //menu.currentPageIndex = 32;
                    //menu.menuInactiveTimer = 50;
                    //break;
                }
                if (menu.menuPages[4].menuElements[1].menuClickedLeft() && menu.currentPageIndex == 42 && menu.menuInactiveTimer == 0)
                {
                    //Some Mode
                    Console.WriteLine("GO back to page 32?");
                    menu.currentPageIndex = 32;
                    menu.menuInactiveTimer = 50;
                    break;
                }
                #endregion

                //Update the elements just.
                for (int j = 0; j < menu.menuPages[i].menuElements.Count; j++)
                {
                    menu.menuPages[i].menuElements[j].updateMenuElement();
                }
            }
        }
        #endregion Menu functions


        #endregion the whole class
    }
       
}