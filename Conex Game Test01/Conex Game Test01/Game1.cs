using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;
using System.Threading;
using System.Media;
using ConexGameLibrary;

namespace Conex_Game_Test01
{
    public partial class MainGame : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager GraphicsDeviceManager;
        public GeneralGameData gameData;
        public GameSegment Segment01;
        public MenuSegment menuSegment;

        public MainGame()
        {
            GraphicsDeviceManager = new GraphicsDeviceManager(this);
            gameData = new GeneralGameData();

            String exePath = System.Reflection.Assembly.GetAssembly(typeof(MainGame)).CodeBase;
            String Dir = Path.GetDirectoryName(exePath);
            if (Dir.StartsWith(String.Format("file")))
                Dir = Dir.Substring(6);

            Console.WriteLine(Dir);

            gameData.SetGameData("C://Users//Conex//Desktop//gamefolder",
                2,
                1920,
                900,
                60,
                false,
                false,
                true,
                (float)5000 / (float)(2050),
                false,
                0.1f,
                1110000.0f,
                "_Lights",
                1.0f,/*120kmh 100/16*/
                5, 7.81f,
                new Vector3[] { new Vector3(0, 830, 0), new
                    Vector3(0, 833, 0), new Vector3(0, 83, 0), new Vector3(0, 3, 0) });
        }

        /*******************************/
        //Menu segment functions here
        /*******************************/
        #region Menu Stuff

        public void StartMenu()
        {
            Console.WriteLine("Starting Menu");
            gameData.ActiveSegment = "Menu";
            this.IsMouseVisible = true;
            this.menuSegment = new MenuSegment();
        }

        public void InitialiseMenu()
        {
            Console.WriteLine("Initialising Menu");
            ApplyGraphicsData(gameData, GraphicsDeviceManager);
            Load_basicMenuData(menuSegment, gameData); //Textures, init shader, font.
            Create_MenuPages(menuSegment, gameData);
            Create_MenuRenderWindow_Players(menuSegment, gameData);
            Load_MenuModels(menuSegment, gameData);
        }

        public void UpdateMenu()
        {
            UpdateMenuFunction(menuSegment);
            UpdateAnimationData_Menu();
        }

        public void DrawMenu()
        {
            Menu_Draw_PlayerModels(menuSegment, gameData);
            DrawMenuWindows(menuSegment);
            Render_Preview_Windows(menuSegment);
        }

        #endregion
        /*******************************/

        /*******************************/
        //Menu segment functions here
        /*******************************/
        #region Game Segment stuff

        public void StartSegment()
        {
            ModelMesh_C3D_IO.Write_C3D_File(string.Empty, null);
            ApplyGraphicsData(gameData, GraphicsDeviceManager);
            gameData.ActiveSegment = "Game";
            gameData.RelativeMapFileAndPath = "NPC_TestGround";
            gameData.AmbientLightAndIntensity = new Vector4(1f, 1f, 1f, 1.0f) * 0.2f;
            gameData.DiffuseLightIntensity = 0.5f;
            gameData.DiffuseLightColor = new Vector4(1.0f, 1.0f, 0.6f, 0.6f);
            gameData.DiffuseLightDirection = -new Vector3(1, -1, 0.5f);
            gameData.SpecularReflectivity = 1.0f;

            Segment01 = new GameSegment();
        }

        public void InitialiseSegment()
        {
            if (gameData.ActiveSegment == "Game")
            {
                CreateSpriteBatch(Segment01, gameData);
                CreateViewports(gameData, Segment01);
                Load_Shaders(Segment01);
                CreatePerspectives(gameData, Segment01);
                InitialsieMirrors();
                InitialiseShadows();
                //CreateTemporarySprites(gameData, Segment01);
                LoadSprite_Light(gameData, Segment01);
                LoadSprite_GunFlash(gameData, Segment01);
                LoadMapMeshes_VB(gameData.GameDirectory, gameData.RelativeMapFileAndPath, Segment01);
                LoadMapMeshes_OLD_loadsintoRam(gameData.GameDirectory, gameData.RelativeMapFileAndPath, Segment01);
                GenerateLightMaps_FromScriptAndBlocks(gameData, Segment01);
                Create_PlayerLoadInfo(Segment01);
                Load_PlayerModelData(Segment01, gameData);
                Create_GunLoadinfoAndProperties(Segment01);
                Load_WeaponModels(Segment01, gameData, GraphicsDevice);
                CreatePlayers(gameData, Segment01);
                LoadMap(gameData, Segment01);
                LoadSoundsToCollection(Segment01, gameData);
                Load_SharedAnimations_Packets(Segment01, gameData);

                CreateNpcs(Segment01);
                LoadNpcNodeMesh(Segment01, gameData);
                LoadLineShader(Segment01, gameData);
                //Loadmap must be called before this                               

                LoadItemsForMap(Segment01, gameData);
                //----------------------------------------//
                CreateHudData(Segment01, gameData);
                initialiseJoystick(Segment01, gameData);
            }
        }

        public void updateSegment()
        {
            UpdateFunctionCounters(gameData);
            UpdatePlayerInput(Segment01.Players, gameData);
            UpdatePlayerAndCamera(Segment01.Players, gameData);
            //UpdateSpriteLightList(gameData,   Segment01);
            Update_PlayerAnimationPlaybackAndEvents(Segment01, gameData);
            Handle_NpcNode_Select(Segment01, gameData);
            UpdateNPC_Behaviour_and_Animation(Segment01, gameData);
            Update_WeaponAndAnimation_World(Segment01, gameData, 0);

            //UpdateAnimationData(  Segment01);
            UpdateItems(Segment01, gameData);
            UpdatePlayerStuff(Segment01, gameData);
            HandlePlayerCollisions(gameData, Segment01);
            Update_FirearmSprites(Segment01, gameData);
            UpdateHudData(Segment01, gameData);

            UpdatePlayerInput_joysticks(Segment01, Segment01.Players, gameData);
        }

        public void DrawSegment()
        {
            DrawMeshgroup(Segment01, gameData, 0);
        }

        #endregion
        /*******************************/


        /*******************************/
        //General Function calls here
        /*******************************/
        #region General Calls

        //Load everything for the segments
        protected override void Initialize()
        {
            //The entry of the game starts with menu of course

            //Menu
            //StartMenu();
            //InitialiseMenu();

            //Game
            StartSegment();
            InitialiseSegment();

            //Editor?
            //StartSEditor(); ???
            //InitialiseEditor(); ???

            base.Initialize();
        }

        //Update game data
        protected override void Update(GameTime gameTime)
        {
            if (gameData.ActiveSegment == "Game")
            {
                updateSegment();
            }
            if (gameData.ActiveSegment == "Menu")
            {
                UpdateMenu();
            }
            if (gameData.ActiveSegment == "Editor")
            {
                //UpdateEditor();
            }
            base.Update(gameTime);
        }

        //Draw game data
        protected override void Draw(GameTime gameTime)
        {
            if (gameData.ActiveSegment == "Menu")
            {
                DrawMenu();
            }
            if (gameData.ActiveSegment == "Editor")
            {
                //DrawEditor();
            }
            if (gameData.ActiveSegment == "Game")
            {
                DrawSegment();
            }
            base.Draw(gameTime);
        }

        #endregion
        /*******************************/
    }


}
