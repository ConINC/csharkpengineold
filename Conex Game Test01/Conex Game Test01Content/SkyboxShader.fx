//SIMPLE DIFFUSE AND AMBIENT SHADER! NOT ACTUALLY THE BLINN MODEL!

float4x4 World;
float4x4 View;
float4x4 Projection;

float4x4 lightWorld;
float4x4 lightView;
float4x4 lightProjection;

float4 AmbientColor;
float AmbientIntensity = 0;

float4 DiffuseLightColor;
float3 DiffuseLightDirection;
float DiffuseLightIntensity;

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Normal: NORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0; //DONT USE MOFO

	float4 Color : COLOR0;
	float3 Normal : TEXCOORD0;
	float4 TextureCoordinate : TEXCOORD1;
	float4 Position4D : TEXCOORD2;
};

texture ModelTexture;

sampler2D textureSampler = sampler_state {
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

texture ShadowMap;

sampler2D textureSampler2 = sampler_state {
    Texture = (ShadowMap); // texture is assigned
    MagFilter = Point;
	MipFilter = Point;
    AddressU = clamp;
    AddressV = clamp;
};

VertexShaderOutput vertexShader1(VertexShaderInput input)
{
	VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

	float3 lookAtVertex = normalize((input.Position - DiffuseLightDirection)); // Direction from camera to the Vertex, is needed

	float3 lightDirection = normalize(DiffuseLightDirection); //This is where the light comes from

	float3 inputNormal = normalize(input.Normal);		//surface Normal Vector
		
	output.Color = DiffuseLightColor;
	output.Normal = input.Normal;
	output.TextureCoordinate.xy = input.TextureCoordinate.xy;
	output.TextureCoordinate.zw = 1;

	output.Position4D = input.Position;
	return output;
}

float4 pixelShader1(VertexShaderOutput input) : COLOR0
{
	// tex2d gets the color of each pixel on the given position using the sampler
	float4 textureColor = tex2D(textureSampler, input.TextureCoordinate);
	float dot_scale =  dot(normalize(input.Normal),normalize(DiffuseLightDirection)) / (length(normalize(input.Normal)) * length(normalize(DiffuseLightDirection)));
	

	float4 diffuseFinal =   DiffuseLightColor*dot_scale*0.1f + (textureColor + textureColor) * (dot_scale * DiffuseLightIntensity);
	float4 ambientFinal =  AmbientIntensity * (textureColor + AmbientColor)/2;
	float4 return_color = ambientFinal + diffuseFinal;
	return saturate(ambientFinal) + saturate(return_color);
}

technique SimpleTexture
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 vertexShader1();
		PixelShader = compile ps_3_0 pixelShader1();
	}
}
//__________________________________________________________________

VertexShaderOutput vertexShader2(VertexShaderInput input)
{
	VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

	output.Color = DiffuseLightColor;
	output.Normal = input.Normal;
	output.TextureCoordinate.xy = input.TextureCoordinate.xy;
	

	output.Color = DiffuseLightColor;
	output.Normal = input.Normal;
	output.TextureCoordinate.xy = input.TextureCoordinate.xy;
	output.TextureCoordinate.zw = 1;

	output.Position4D = input.Position;
	return output;
}

float4 pixelShader2(VertexShaderOutput input) : COLOR0
{	
	float4 textureColor = tex2D(textureSampler, input.TextureCoordinate);
	
	return textureColor;
}


technique SimpleColor
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 vertexShader2();
		PixelShader = compile ps_3_0 pixelShader2();
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
struct VertexShaderOutputLight
{
	float4 Position : POSITION0; //DONT USE MOFO
	float4 Position4D : TEXCOORD0; 	
	float4 Color : Color0;
};


VertexShaderOutputLight vertexShaderLight(float4 inPosition : POSITION0)
{
	VertexShaderOutputLight output;
		
    float4 worldPosition = mul(inPosition, lightWorld);
    float4 viewPosition = mul(worldPosition, lightView);
    output.Position = mul(viewPosition, lightProjection);
			
	output.Position4D = output.Position;	
	
	output.Color = float4(1,1,1,1);
	return output;
}

float4 pixelShaderLight(VertexShaderOutputLight input) : COLOR0
{
	return input.Position4D;
}

technique Shadow
{
	pass PassShadowMap
	{
		VertexShader = compile vs_3_0 vertexShaderLight();
		PixelShader = compile ps_3_0 pixelShaderLight();
	}
}



