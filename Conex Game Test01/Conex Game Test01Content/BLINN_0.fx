float4x4 World;
float4x4 View;
float4x4 Projection;

//Ambient Light //
float4 AmbientLightAndIntensity;

//General Field, many uses
float3 CameraPosition;

//Diffuse Lighting related Fields
float4 DiffuseLightColorAndIntensity;

float3 DiffuseLightDirection; //Not sure here.
float normalPower;

//Texture fields
texture ModelTexture;
texture NormalTexture;
texture SpecularTexture;

//Texture Samplers for the 3 main maps
sampler2D colorSampler = sampler_state
{
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
	MaxAnisotropy = 8;
    AddressV = Wrap;
};

sampler2D normalSampler = sampler_state
{
    Texture = (NormalTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler2D specularSampler = sampler_state
{
    Texture = (SpecularTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Shader Structs
struct VertexShaderInput
{
	float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;	
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
	float4 DiffuseLightFromVertexShader : TEXCOORD1;
	float3 Position3D : TEXCOORD2;
};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
VertexShaderOutput VS(VertexShaderInput input)
{
	VertexShaderOutput output;
	
	//For textures
	float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);


	output.Position3D = input.Position;
	output.TextureCoordinate = input.TextureCoordinate;
	output.Normal = input.Normal;


	output.Tangent = input.Tangent;
	output.BiNormal = input.BiNormal;
	
	float dotd = dot(DiffuseLightDirection, output.Normal);
	if(dotd < 0)
		dotd = 0;

	output.DiffuseLightFromVertexShader =  dotd * DiffuseLightColorAndIntensity;
			
	return output;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
float4 PS(VertexShaderOutput input) : COLOR0
{	
	
	float4 textureColor = tex2D(colorSampler,input.TextureCoordinate * float2(1,-1));
	float4 lightcolor = input.DiffuseLightFromVertexShader + AmbientLightAndIntensity;
	lightcolor.w = 1;
	return textureColor * lightcolor;
}

//------------------------------------------------------------------------------
technique PHONG_WITH_CUBE
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}
//-------------------------------------------------------------
