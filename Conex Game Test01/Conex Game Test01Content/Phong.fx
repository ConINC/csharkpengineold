float4x4 World;
float4x4 View;
float4x4 Projection;

//Ambient Light //
float4 AmbientLightAndIntensity;

//General Field, many uses
float3 CameraPosition;

//Diffuse Lighting related Fields
float4 DiffuseLightColor;
float DiffuseLightIntensity;
float3 DiffuseLightDirection; //Not sure here.

//Texture fields
texture ModelTexture;

texture NormalTexture;
float NormalFactor = 0.2f;

texture SpecularTexture;
float SpecularRoughness = 40;
float3 CameraLookAt;
float SpecularReflectivity;

//Flashlight related Fields
float4x4 LightView;
float4x4 LightProjection;
texture flashLight;

//ShadowRelated
//The viewmatrix of shadow is the Light that casts it
float4x4 ShadowView;
float4x4 ShadowProjection;
texture ShadowTexture;

//Point Light Fields here
float Brightnesses[100];
float3 LightPoss[100];
float4 LightColors[100];

int PhysblockSelected = 0;
int LinearLightFalloff = 0;

float Transparency;

//------------------------------------------------------------------------------
//Various Samplers for everything
sampler2D flashLightSampler = sampler_state
{
    Texture = (flashLight); // texture is assigned
    MagFilter = Point;
	MipFilter = Point;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler2D ShadowSampler = sampler_state
{
    Texture = (ShadowTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = clamp;
    AddressV = clamp;
};

sampler2D textureSampler = sampler_state
{
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
	MaxAnisotropy = 8;
    AddressV = Wrap;
};

sampler2D normalSampler = sampler_state
{
    Texture = (NormalTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler2D specularSampler = sampler_state
{
    Texture = (SpecularTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Shader Structs
struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 PositionAcces : Position;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

//------------------------------------------------------------------------------
//Shader Structs
struct VertexShaderInputSimple
{
	float4 Position : POSITION0;	
	float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{

	float4 Position : POSITION0;
	float4 PositionAcces : TEXCOORD2;
	float4 PositionProjectiveLight : TEXCOORD4;
	float3 Normal : TEXCOORD1;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
	float4 DiffuseLightFromVertexShader : TEXCOORD3;
	float4 lpos : TEXCOORD5;
	float4 Position3D : TEXCOORD6;
};
//------------------------------------------------------------------------------

struct VertexShaderOutputSimple
{
	float4 Position : POSITION0;	
	float2 TextureCoordinate : TEXCOORD0;

};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Vertex Shader function, is used in both, pretty standard stuff goes on here
VertexShaderOutput VertexShaderFunction_Phong(VertexShaderInput input)
{
	VertexShaderOutput output;
	
	//For textures
	float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

	//For Lights
	float4 LightPosWorld = mul(input.Position, World);
    float4 viewPositionLight = mul(LightPosWorld,LightView);
    output.PositionProjectiveLight = mul(viewPositionLight, LightProjection );

	//-----------------Shadow----------------//	

    output.lpos = mul( input.Position, mul(World, mul(ShadowProjection, ShadowView) ) );
	output.Position3D = input.Position;

	//-----------------EndShadow----------------//


	output.TextureCoordinate = input.TextureCoordinate;
	output.Normal = input.Normal;
	output.PositionAcces = input.Position;

	output.Tangent = input.Tangent;
	output.BiNormal = input.BiNormal;


	float dotd = dot(DiffuseLightDirection, output.Normal);
	if(dotd < 0)
		dotd *=1;

	output.DiffuseLightFromVertexShader =  dotd * DiffuseLightIntensity * DiffuseLightColor;
	
    output.PositionProjectiveLight = mul(mul(worldPosition, LightView), LightProjection);
			
	return output;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Vertex Shader function, is used in both, pretty standard stuff goes on here
VertexShaderOutputSimple VertexShaderFunction_Simple(VertexShaderInputSimple input)
{
	VertexShaderOutputSimple output;
	
	//For textures
	float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
		
	output.TextureCoordinate = input.TextureCoordinate;
	
	return output;
}
//------------------------------------------------------------------------------
 
//------------------------------------------------------------------------------
//PixelShader ((Phong))
float4 PixelShader_Phong(VertexShaderOutput input) : COLOR0
{
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//
	float2 NormalChange_XY = normalize(tex2D(normalSampler, input.TextureCoordinate.xy* float2(1,-1) )) - float2(0.5,0.5);
	
	//X is sideways, will be used for binormal,  Y for tangent change
	//new Normal will be (X * binormal + normal, X * Tangent + normal)*factor

	//input.Normal += normalize(input.Tangent) * 0.05 * NormalChange_XY.x +  normalize(input.BiNormal) * 0.05 * NormalChange_XY.y;
	
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//


	//-----------------------Diffuse----------------------//
	float dotDiffuseWithBump =  dot(DiffuseLightDirection, input.Normal);
	if(dotDiffuseWithBump < 0)
		dotDiffuseWithBump*=-1;

	float dotd = dot(DiffuseLightDirection, input.Normal);
	if(dotd < 0)
		dotd = 0;

	float4 DiffuseLight =  dotd * DiffuseLightIntensity * DiffuseLightColor;
	//-----------------------Diffuse----------------------//


	//-------------------------Specularity---------------------//
	float4 SpecularColor = tex2D(specularSampler, input.TextureCoordinate.xy* float2(1,-1));

	float3 V = CameraPosition - input.PositionAcces.xyz;
	float3 LightReflect = reflect(DiffuseLightDirection, input.Normal);
	
	float Dot = dot(LightReflect,CameraLookAt);
	if(Dot > 0)
	{
		Dot = Dot;
	}
	Dot = pow( Dot / (length(LightReflect)* length(CameraLookAt)), SpecularRoughness);

	float Specularity =  (SpecularRoughness)/6.28f * Dot;
	//-------------------------Specularity---------------------//
	
		
	
		
	float len = length(input.PositionAcces - CameraPosition);

	float4 FinalLampCombined = float4(0,0,0,0);

	int lightCounter = 0;
	float Distances[50];

	
	for(;lightCounter < 35; lightCounter++)
	{
		Distances[lightCounter] = length(input.PositionAcces.xyz - LightPoss[lightCounter].xyz);
		FinalLampCombined += pow(2.71f, (- (Distances[lightCounter]/Brightnesses[lightCounter])+5))*LightColors[lightCounter];		
	}
	
	float4 samplingPos = input.PositionProjectiveLight* float4(0.1,0.1,1,1) + float4(0.5,0.5,0,0);
	
	float4 LightMap = tex2D(flashLightSampler, samplingPos.xy) * 2 * pow(2.71, (-len/5));

	float4 textureColor = tex2D(textureSampler,input.TextureCoordinate * float2(1,-1));
	
	//DIFUSE MISSING
	float4 Final = (AmbientLightAndIntensity + DiffuseLight)*textureColor + saturate(Specularity)* SpecularReflectivity + FinalLampCombined*textureColor;
	return Final;// +LightMap;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//PixelShader ((Phong))
float4 PixelShader_Phong_LightLinear(VertexShaderOutput input) : COLOR0
{
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//
	float2 NormalChange_XY = normalize(tex2D(normalSampler, input.TextureCoordinate.xy* float2(1,-1) )) - float2(0.5,0.5);
	
	//X is sideways, will be used for binormal,  Y for tangent change
	//new Normal will be (X * binormal + normal, X * Tangent + normal)*factor

	//input.Normal += normalize(input.Tangent) * 0.05 * NormalChange_XY.x +  normalize(input.BiNormal) * 0.05 * NormalChange_XY.y;
	
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//


	//-----------------------Diffuse----------------------//
	float dotDiffuseWithBump =  dot(DiffuseLightDirection, input.Normal);
	if(dotDiffuseWithBump < 0)
		dotDiffuseWithBump*=-1;

	float dotd = dot(DiffuseLightDirection, input.Normal);
	if(dotd < 0)
		dotd = 0;

	float4 DiffuseLight =  dotd * DiffuseLightIntensity * DiffuseLightColor;
	//-----------------------Diffuse----------------------//


	//-------------------------Specularity---------------------//
	float4 SpecularColor = tex2D(specularSampler, input.TextureCoordinate.xy* float2(1,-1));

	float3 V = CameraPosition - input.PositionAcces.xyz;
	float3 LightReflect = reflect(DiffuseLightDirection, input.Normal);
	
	float Dot = dot(LightReflect,CameraLookAt);
	if(Dot > 0)
	{
		Dot = Dot;
	}
	Dot = pow( Dot / (length(LightReflect)* length(CameraLookAt)), SpecularRoughness);

	float Specularity =  (SpecularRoughness)/6.28f * Dot;
	//-------------------------Specularity---------------------//
	
	float len = length(input.PositionAcces - CameraPosition);

	float4 FinalLampCombined = float4(0,0,0,0);

	int lightCounter = 0;
	float Distances[50];

	float dot_pointlight = 0;
	
	for(;lightCounter < 25; lightCounter++)
	{
		float dot_pointlight = dot(input.Normal, normalize(LightPoss[lightCounter].xyz - input.PositionAcces.xyz));
		Distances[lightCounter] = length(input.PositionAcces.xyz - LightPoss[lightCounter].xyz);
		FinalLampCombined += ( 1/Distances[lightCounter]) * Brightnesses[lightCounter] * LightColors[lightCounter]*dot_pointlight;

	}
	
	float4 samplingPos = input.PositionProjectiveLight* float4(0.1,0.1,1,1) + float4(0.5,0.5,0,0);
	
	float4 LightMap = tex2D(flashLightSampler, samplingPos.xy) * 2 * pow(2.71, (-len/5));

	float4 textureColor = tex2D(textureSampler,input.TextureCoordinate * float2(1,-1));
	
	//DIFUSE MISSING
	float4 Final = (AmbientLightAndIntensity + DiffuseLight)*textureColor + saturate(Specularity)* SpecularReflectivity + FinalLampCombined*textureColor;
	return Final;// +LightMap;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//PixelShader ((Phong))
float4 PixelShader_Phong_SpriteLightsOnly(VertexShaderOutput input) : COLOR0
{
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//
	float2 NormalChange_XY = normalize(tex2D(normalSampler, input.TextureCoordinate.xy* float2(1,-1) )) - float2(0.5,0.5);
	
	//X is sideways, will be used for binormal,  Y for tangent change
	//new Normal will be (X * binormal + normal, X * Tangent + normal)*factor

	input.Normal += normalize(input.Tangent) * 0.05 * NormalChange_XY.x +  normalize(input.BiNormal) * 0.05 * NormalChange_XY.y;
	
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//

	float4 FinalLampCombined = float4(0,0,0,0);

	int lightCounter = 0;
	float Distances[50];

	float dot_pointlight = 0;
	
	for(;lightCounter < 30; lightCounter++)
	{
		float dot_pointlight = dot(input.Normal, normalize(LightPoss[lightCounter].xyz - input.PositionAcces.xyz));
		Distances[lightCounter] = length(input.PositionAcces.xyz - LightPoss[lightCounter].xyz);
		FinalLampCombined += ( 1/Distances[lightCounter]) * Brightnesses[lightCounter] * LightColors[lightCounter]*dot_pointlight;

	}	
	float4 textureColor = tex2D(textureSampler,input.TextureCoordinate * float2(1,-1));
		
	//DIFUSE MISSING
	float4 Final = FinalLampCombined*textureColor;
	return Final;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//This will be the normal Phong shader, it includes Ambient, Diffuse, Specular Lighting
//It also includes the 40 Point Lights, Bumpmaps, Specular maps and ShadowMapping or LightMapping is planned
technique Phong
{
	pass Pass_0
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 PixelShader_Phong();
	}
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//This will be the normal Phong shader, it includes Ambient, Diffuse, Specular Lighting
//It also includes the 40 Point Lights, Bumpmaps, Specular maps and ShadowMapping or LightMapping is planned
technique Phong_LinearLight
{
	pass Pass_0
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 PixelShader_Phong_LightLinear();
	}
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//This will be the normal Phong shader, it includes Ambient, Diffuse, Specular Lighting
//It also includes the 40 Point Lights, Bumpmaps, Specular maps and ShadowMapping or LightMapping is planned
technique Phong_SpriteLightsOnly
{
	pass Pass_0
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 PixelShader_Phong_SpriteLightsOnly();
	}
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//PixelShader ((PixelShader_MapBlocks))
float4 PixelShader_MapBlocks(VertexShaderOutput input) : COLOR0
{		
		return input.DiffuseLightFromVertexShader + AmbientLightAndIntensity;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//To Render Physics objects that are normaly hidden
technique PhysicsBlocks
{	
	pass Pass_0
	{		
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 PixelShader_MapBlocks();
	}	
}

//-----------------------------------------------------------------
float4 SimpleTexLighting(VertexShaderOutput input) : COLOR0
{

	float4 textureColor = tex2D(textureSampler, input.TextureCoordinate* float2(1,-1));	
	return textureColor*(input.DiffuseLightFromVertexShader + AmbientLightAndIntensity);
}

technique SimpleTextureLighting
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 SimpleTexLighting();
	}
}
//-----------------------------------------------------------------

//-----------------------------------------------------------------
float4 SimpleTex(VertexShaderOutputSimple input) : COLOR0
{
	float4 textureColor = tex2D(textureSampler, input.TextureCoordinate* float2(1,-1));
	//textureColor.rgba *= Transparency;
	return textureColor;
}

technique SimpleTexture
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Simple();
		PixelShader = compile ps_3_0 SimpleTex();
	}
}
//-----------------------------------------------------------------


//-----------------------------------------------------------------
float4 ShadowMap(VertexShaderOutput input) : COLOR0
{	
	float depth;	
	depth = 1 - (input.PositionAcces.z / input.PositionAcces.w);
	return float4(depth,depth,depth,1);
}

technique ShadowTechnique
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 VertexShaderFunction_Phong();
		PixelShader = compile ps_3_0 ShadowMap();
	}
}
//-------------------------------------------------------------

float4 SimpleShadowTesting(VertexShaderOutput input) : COLOR0
{
}