float4x4 World;
float4x4 View;
float4x4 Projection;

float4x4 BonexMatrices[32];
float4 BoneIndices; //This allows 4 vertex per bone
float4 BoneWeights; //4 Weights for the 4 vertices
int BoneCount;
bool AllowSkinning;

// TODO: add effect parameters here.

struct VertexShaderInput
{
    float4 Position : POSITION0;
	float4 PositionUse : TextureCoordinate0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

	float4 PositionSkinned = float4(0,0,0,0);

	int perJointCounter= 0;

	for(;perJointCounter < 4; perJointCounter++)
	{

	}

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	   
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	
    return float4(1, 1, 1, 1);
}

technique Technique1
{
    pass Pass1
    {
        // TODO: set renderstates here.

        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
