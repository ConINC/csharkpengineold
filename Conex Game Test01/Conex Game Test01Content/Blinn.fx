float4x4 World;
float4x4 View;
float4x4 Projection;

float4x4 normalmatrix1;
float4x4 normalmatrix2;
float4x4 normalmatrix3;

//Ambient Light //
float4 AmbientLightAndIntensity;

//General Field, many uses
float3 CameraPosition;

//Diffuse Lighting related Fields
float4 DiffuseLightColor;
float DiffuseLightIntensity;
float3 DiffuseLightDirection; //Not sure here.

//Texture fields
texture ModelTexture;

texture NormalTexture;
float NormalFactor = 0.2f;

texture SpecularTexture;
float SpecularRoughness = 32;
float SpecularIntensity = 0; 


//------------------------------------------------------------------------------
//Various Samplers for everything

sampler2D textureSampler = sampler_state
{
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
	MaxAnisotropy = 8;
    AddressV = Wrap;
};

sampler2D normalSampler = sampler_state
{
    Texture = (NormalTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

sampler2D specularSampler = sampler_state
{
    Texture = (SpecularTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Shader Structs
struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 PositionAcces : Position;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct PixelShaderInput
{
	float4 Position : POSITION0;
	float4 PositionAcces : TEXCOORD2;
	float3 Normal : TEXCOORD1;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
	float4 DiffuseLightFromVertexShader : TEXCOORD3;

};
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Vertex Shader function, is used in both, pretty standard stuff goes on here
PixelShaderInput Blinn_vs(VertexShaderInput input)
{
	PixelShaderInput output;
	
	//For textures

	float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	
	output.TextureCoordinate = input.TextureCoordinate;
	output.Normal = input.Normal;
	output.PositionAcces = input.Position;
	

	output.Tangent = input.Tangent;
	output.BiNormal = input.BiNormal;

	float dotd = dot(DiffuseLightDirection, output.Normal);
	if(dotd < 0)
		dotd *= 0;

	output.DiffuseLightFromVertexShader =  dotd * DiffuseLightIntensity * DiffuseLightColor;
	
	return output;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//Blinn PS
float4 Blinn_ps(PixelShaderInput input) : COLOR0
{
	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//
	float2 NormalChange_XY = normalize(tex2D(normalSampler, input.TextureCoordinate.xy* float2(1,-1) )) - float2(0.5,0.5) * 2;
	//the amount range -0.5 to 0.5
	
	

	//X is sideways, will be used for binormal,  Y for tangent change
	//new Normal will be (X * binormal + normal, X * Tangent + normal)*factor

	float4 normal;
	normal.xyz = input.Normal.xyz;
	normal.w = 1;

	float4 u_dir = NormalChange_XY.x  * mul(normalmatrix1,normal);
	float4 v_dir = NormalChange_XY.y  * mul(normalmatrix2,normal);
	input.Normal.x += u_dir;
	input.Normal.y += v_dir;
	input.Normal = normalize(input.Normal);

	//-----------------------BUMPMAP--------------------------------------------------------------------------------------//

	//-------------------------Specularity---------------------//
	float4 SpecularColor = tex2D(specularSampler, input.TextureCoordinate.xy* float2(1,-1));

	float3 V = normalize(input.PositionAcces.xyz - CameraPosition.xyz);
	float3 L = normalize(-DiffuseLightDirection.xyz);
		
	float top = dot(V+L,input.Normal);
	if(top < 0)
		top = 0;

	float bottom = length(V+L) * length(input.Normal);
	if(SpecularColor.x < 0.02)
		SpecularColor.x = 1;

	float Specularity = SpecularColor.x * SpecularIntensity * pow( abs((top / bottom)), SpecularRoughness);
	
	float4 textureColor = tex2D(textureSampler,input.TextureCoordinate * float2(1,-1));	

	//--------------------------------------------------------//	

	float4 Final = (AmbientLightAndIntensity + input.DiffuseLightFromVertexShader)*textureColor + Specularity;
	return Final;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//This will be the normal Phong shader, it includes Ambient, Diffuse, Specular Lighting
technique Blinn
{
	pass Pass_0
	{
		VertexShader = compile vs_3_0 Blinn_vs();
		PixelShader = compile ps_3_0 Blinn_ps();
	}
}
//------------------------------------------------------------------------------
