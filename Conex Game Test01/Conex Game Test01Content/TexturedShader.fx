//SIMPLE DIFFUSE AND AMBIENT SHADER! NOT ACTUALLY THE BLINN MODEL!

float4x4 World;
float4x4 View;
float4x4 Projection;

texture ModelTexture;

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float4 Normal: NORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float4 TextureCoordinate : TEXCOORD1;
};

sampler2D textureSampler = sampler_state {
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
    AddressU = Wrap;
    AddressV = Wrap;
};

VertexShaderOutput vertexShader1(VertexShaderInput input)
{
	VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);

    output.Position = mul(viewPosition, Projection);	
	output.TextureCoordinate = float4(0,0,0,0);	
	output.TextureCoordinate.xy = input.TextureCoordinate.xy;

	return output;
}

float4 pixelShader1(VertexShaderOutput input) : COLOR0
{
	float4 textureColor = tex2D(textureSampler, input.TextureCoordinate);	

	return textureColor;
}

technique SimpleTexture
{
	pass PassColor
	{
		VertexShader = compile vs_3_0 vertexShader1();
		PixelShader = compile ps_3_0 pixelShader1();
	}
}
