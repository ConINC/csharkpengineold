//-----------------------------------------------------------------------------
// DrawModel.fx
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 LightViewProj;

float4x4 normalmatrix1;
float4x4 normalmatrix2;
float4x4 normalmatrix3;

//Ambient Light //
float4 AmbientLightAndIntensity;

//General Field, many uses
float3 CameraPosition;

//Diffuse Lighting related Fields
float4 DiffuseLightColor;
float DiffuseLightIntensity;
float3 DiffuseLightDirection; //Not sure here.

float ShadowOn = 0;

//Texture fields
texture ShadowMap;
texture ModelTexture;

texture NormalTexture;
float NormalFactor = 0.2f;

texture SpecularTexture;
float SpecularRoughness = 32;
float SpecularIntensity = 0; 

float3 LightDirection;
float DepthBias = 0.006f;


//------------------------------------------------------------------------------
//Various Samplers for everything

sampler2D ShadowMap01_sampler = sampler_state
{
	MagFilter = Point;
	MipFilter = Point;
    Texture = (ShadowMap); // texture is assigned
};

sampler2D textureSampler = sampler_state
{
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Point;
	MipFilter = Point;
    AddressU = Wrap;
	MaxAnisotropy = 8;
    AddressV = Wrap;
};

struct DrawWithShadowMap_VSIn
{
    float4 Position : POSITION0;
	float4 PositionAcces : Position;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct DrawWithShadowMap_VSOut
{
	float4 Position : POSITION0;
	float3 Tangent : TEXCOORD0;
	float3 BiNormal : TEXCOORD1;
	float3 Normal   : TEXCOORD6;
	float4 PositionAcces : TEXCOORD2;
	float2 TextureCoordinate : TEXCOORD3;
	float4 DiffuseLightFromVertexShader : TEXCOORD4;
	float4 WorldPos : TEXCOORD5;
};


// Draws the model with shadows
DrawWithShadowMap_VSOut DrawWithShadowMap_VertexShader(DrawWithShadowMap_VSIn input)
{
    DrawWithShadowMap_VSOut Output;

    float4x4 WorldViewProj = mul(mul(World, View), Projection);
    
    // Transform the models verticies and normal
    Output.Position = mul(input.Position, WorldViewProj);
    Output.Normal =  normalize(mul(input.Normal, World));
    Output.TextureCoordinate = input.TextureCoordinate;
    
    // Save the vertices postion in world space
    Output.WorldPos = mul(input.Position, World);
    
	Output.PositionAcces = Output.Position;
	
	Output.Tangent = normalize(mul(input.Tangent, World));
	Output.BiNormal = normalize(mul(input.BiNormal, World));

	float dotd = dot(DiffuseLightDirection, Output.Normal);
	if(dotd > 0.5)
		dotd = 1;
	if(dotd <= 0.5)
		dotd = 0;


	Output.DiffuseLightFromVertexShader =  dotd * DiffuseLightIntensity * DiffuseLightColor;

    return Output;
}

// Determines the depth of the pixel for the model and checks to see 
// if it is in shadow or not
float4 DrawWithShadowMap_PixelShader(DrawWithShadowMap_VSOut input) : COLOR
{
    //-----------------------------ShadowMappign-----------------------//
    // Find the position of this pixel in light space
    float4 lightingPosition = mul(input.WorldPos, LightViewProj);
    
    // Find the position in the shadow map for this pixel
    float2 ShadowTexCoord = 0.5 * lightingPosition.xy / lightingPosition.w + float2( 0.5, 0.5 );
    ShadowTexCoord.y = 1.0f - ShadowTexCoord.y;

    // Get the current depth stored in the shadow map
    float shadowdepth = tex2D(ShadowMap01_sampler, ShadowTexCoord).r;    
    
    // Calculate the current pixel depth
    // The bias is used to prevent folating point errors that occur when
    // the pixel of the occluder is being drawn
    float ourdepth = (lightingPosition.z / 100) - DepthBias;
    
    // Check to see if this pixel is in front or behind the value in the shadow map
	//-----------------------------ShadowMappign-----------------------//

	
	float4 textureColor = tex2D(textureSampler,input.TextureCoordinate * float2(1,-1));	

	//--------------------------------------------------------//
	
	float dotd = dot(DiffuseLightDirection, input.Normal);
	if(dotd > 0.5)
		//dotd = 1;

	if(dotd < 0.5 && dotd > 0.3)
		dotd = 0.45;

	if(dotd < 0.3 && dotd > 0)
		dotd = 0.15;
	if(dotd < 0)
		dotd = 0;
	
	float4 DiffuseLightFromVertexShader =  dotd * DiffuseLightIntensity * DiffuseLightColor;
	
	float4 Final = (AmbientLightAndIntensity + DiffuseLightFromVertexShader)*textureColor;// + Specularity;
    
	// Check to see if this pixel is in front or behind the value in the shadow map
	if (shadowdepth < ourdepth && ShadowOn > 0.5f)
    {
        return AmbientLightAndIntensity * textureColor;
    }

    return Final;
}

// Technique for drawing with the shadow map
technique DrawWithShadowMap
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 DrawWithShadowMap_VertexShader();
        PixelShader = compile ps_3_0 DrawWithShadowMap_PixelShader();
    }
}
