float4x4 World;
float4x4 View;
float4x4 Projection;

//------Skinning Data-------------//

float4x4 BoneMatrices[32];
float4x4 IT_BoneMatrices[32];
int BoneIndices[4];
float boneWeights[4];
int BoneCount;
//--------------------------------//


//------Lighting------------------//
float4 AmbientLight;
float3 DiffuseLightDirection;
float4 DiffuseLightColor;
float  DiffuseLightPower;

//--------------------------------//

//-------TEXTURES----------------//
Texture ModelTexture;
Texture NormalTexture;
Texture SpecularTexture;



sampler2D DiffuseSampler = sampler_state
{
    Texture = (ModelTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;

};

sampler2D NormalSampler = sampler_state
{
    Texture = (NormalTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;

};

sampler2D SpecularSampler = sampler_state
{
    Texture = (SpecularTexture); // texture is assigned
    MagFilter = Linear;
	MipFilter = Linear;
};


struct VertexShaderInput
{
    float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float3 BiNormal : BINORMAL0;
	float2 TextureCoordinate : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float4 PositionAcces : POSITION1;
	float3 Normal : NORMAL0;
	float3 Tangent : TANGENT0;
	float2 TextureCoordinate : TEXCOORD0;
	float DiffuseLightDot : TEXCOORD1;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
	
	float4 Position_Transformed = float4(0,0,0,0);
	float3 Normal_Transformed = float3(0,0,0);
	float3 Tangent_Transformed = float3(0,0,0);
	//The biNormal is simply the Cross of normal/tangent..
	int index;
	for(index = 0; index < 4; index++)
	{
		Position_Transformed +=  mul(input.Position,BoneMatrices[index]) * boneWeights[index];
		Normal_Transformed +=  mul(input.Normal,IT_BoneMatrices[index]) * boneWeights[index];
		Tangent_Transformed +=  mul(input.Tangent,IT_BoneMatrices[index]) * boneWeights[index];
	}
	
	output.Tangent = Tangent_Transformed;
	output.Normal = Normal_Transformed;	
	output.DiffuseLightDot = dot(normalize(DiffuseLightDirection), Normal_Transformed);	
	

	// 3d to view space
	output.PositionAcces = Position_Transformed;

    float4 viewPosition = mul(Position_Transformed, View);
    output.Position = mul(viewPosition, Projection);
	
	output.TextureCoordinate = float2(1,1);

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float4 TextureColor = tex2D(DiffuseSampler, input.TextureCoordinate.xy* float2(1,-1));
	float4 NormalPixel = tex2D(NormalSampler, input.TextureCoordinate.xy* float2(1,-1));
	float4 SpecularPixel = tex2D(SpecularSampler, input.TextureCoordinate.xy* float2(1,-1));

    return AmbientLight + TextureColor * (AmbientLight + DiffuseLightColor * input.DiffuseLightDot * DiffuseLightPower);
}

technique Blinn
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}
